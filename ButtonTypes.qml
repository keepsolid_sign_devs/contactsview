pragma Singleton
import QtQuick 2.7

Item {
    id: buttonTypes
    readonly property ButtonStateStyle primary: ButtonStateStyle {
        disabledColorBkg: "#2980cc"
        clickedColorBkg: "#2370b3"
        hoveredColorBkg: "#2e90e6"
        normalColorBkg: "#2980cc"
        textColor: "white"
        disabledOpacity: 0.4
    }
    readonly property ButtonStateStyle secondary: ButtonStateStyle {
        disabledColorBkg: "#ebebeb"
        clickedColorBkg: "#dedede"
        hoveredColorBkg: "#f5f5f5"
        normalColorBkg: "#ebebeb"
        textColor: "#ebebeb"
        disabledOpacity: 0.4
    }
    readonly property ButtonStateStyle cta: ButtonStateStyle {
        disabledColorBkg: "#ebebeb"
        clickedColorBkg: "#dedede"
        hoveredColorBkg: "#f5f5f5"
        normalColorBkg: "#ebebeb"
        textColor: "white"
        disabledOpacity: 0.4
    }
    readonly property ButtonStateStyle warning: ButtonStateStyle {
        disabledColorBkg: "#ebebeb"
        clickedColorBkg: "#dedede"
        hoveredColorBkg: "#f5f5f5"
        normalColorBkg: "#ebebeb"
        textColor: "#f02b2b"
        disabledOpacity: 0.4
    }
}
