#include <QColor>
#include <QQuickView>
#include <QQuickWidget>
#include <QWidget>
#include <QStyle>
#include <QApplication>
#include <QDesktopWidget>
#include <QQmlComponent>
#include <cmath>
#include <random>

#include "contactscontroller.h"

ContactsController::ContactsController(QObject* _parent) : QObject(_parent)
{
    registerTypes();
    fillData();
    setupUI();
    setContactsModelProperty();
    setupConnections();
}

ContactsController::~ContactsController()
{
}

void ContactsController::registerTypes()
{
    qmlRegisterType<ContactModel>("com.contacts", 1, 0, "ContactModel");
    qmlRegisterType<ContactListModel>("com.contacts", 1, 0, "ContactListModel");
    qmlRegisterType<ContactSortFilterModel>("com.contacts", 1, 0, "ContactSortFilterModel");
}

void ContactsController::setupUI()
{
    QQmlComponent newcomp(&engine, QUrl(QLatin1String("qrc:/ContactScreen.qml")), parent());
    mainView = newcomp.create();
    mainView->setProperty("parent", QVariant::fromValue<QObject*>(parent()));
}

void ContactsController::setupConnections()
{
    QObject *rightBtn = mainView->findChild<QObject*>(QString("rightBtn"));

    QObject::connect(rightBtn, SIGNAL(deleteContact(int)), this, SLOT(onDeleteContact(int)));
    QObject::connect(rightBtn, SIGNAL(editContact(int)), this, SLOT(onEditContact(int)));
}

void ContactsController::onAddedContact(ContactModel* contact)
{
    qDebug("ContactsController::onAddedContact()");
    contactsModel.addContact(contact->contactId(), contact->firstName(), contact->lastName(), contact->email(), contact->company(), contact->iconSource(), contact->color(), contact->status());
}

void ContactsController::onDeleteContact(int index)
{
    qDebug("onDeleteContact at index %d", index);
    contactsModel.removeContactAt(index);
}

void ContactsController::onEditContact(int index)
{
    qDebug("onEditContact at index %d", index);
}

void ContactsController::fillData()
{
    long id = 1;
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "JohnJohnJohn", "Doe", "example@gmail.com", "KeepSolidDoe", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "JohnJohnJohn", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "JohnJohnJohn", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "JohnJohnJohn", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "JohnJohnJohn", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "sad_person", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "sad_person", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "sad_person", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "sad_person", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
}

void ContactsController::setContactsModelProperty()
{
    proxyModel.setSourceModel(&contactsModel);
    proxyModel.setFilterRole(ContactListModel::ContactRoles::LastNameRole);
    proxyModel.setFilterCaseSensitivity(Qt::CaseInsensitive);
    proxyModel.setSortRole(ContactListModel::ContactRoles::FirstNameRole);
    proxyModel.setDynamicSortFilter(true);
    proxyModel.sort(0);

    engine.rootContext()->setContextProperty("contactListModel", &contactsModel);
}

void ContactsController::onFilterContacts(QString name)
{
    proxyModel.setFilterFixedString(name);
}
