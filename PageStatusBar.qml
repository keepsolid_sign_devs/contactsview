import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Effects 1.0
import "./buttons" as Buttons

Rectangle {
    id: main
    property int currentPageNumber: 0
    property int pagesCount: 0
    signal zoomValueChanged(int value)

    height: 25
    width: parent? parent.width : 0
    color: "#b3f9f9f9"
    focus: true

    FontLoader {
        id: openSansRegular
        source: "OpenSansFonts/OpenSans-Regular.ttf"
    }

    function changeCurrentPageNumber(number) {
        currentPageNumber = number
    }

    function changePagesCount(count) {
        pagesCount = count
    }

    MouseArea {
        anchors.fill: parent
    }

    Shortcut {
        sequence: StandardKey.ZoomIn
        onActivated: control.increase()
    }

    Shortcut {
        sequence: StandardKey.ZoomOut
        onActivated: control.decrease()
    }

    Rectangle {
        id: divider
        height: 1
        width: parent.width
        color: "#dbdbdb"
        anchors.top: parent.top
    }

    RowLayout {
        anchors.fill: parent
        anchors.top: divider.bottom
        spacing: 0

        Label {
            id: pagesLabel
            Layout.alignment: Qt.AlignVCenter
            Layout.minimumWidth: implicitWidth
            anchors { left: parent.left; leftMargin: 12; verticalCenter: parent.verticalCenter }
            text: "Page " + currentPageNumber + " of " + pagesCount
            font.pointSize: 10
            color: "#444444"
            clip:true
            font.family: openSansRegular.name
        }

        Item {
            height: 1
            Layout.fillWidth: true
        }

        Buttons.ImageButton {
            height: 12
            width: 12
            image.source: "contacts/minus"
            anchors.verticalCenter: parent.verticalCenter

            onClicked: {
                control.decrease()
            }
        }


        Item {
            height: 1
            width: 5
        }

        Slider {
            id: control
            height: parent.height
            width: 100
            from: 0
            value: 100
            to: 200
            stepSize: 5
            snapMode: Slider.SnapAlways

            background: Rectangle {
                x: control.leftPadding
                y: control.topPadding + control.availableHeight / 2 - height / 2
                implicitWidth: parent.width
                implicitHeight: 1
                width: control.availableWidth
                height: implicitHeight
                radius: 2
                antialiasing: true
                color: "#ababab"
            }

            handle: Rectangle {
                x: control.leftPadding + control.visualPosition * (control.availableWidth - width)
                y: control.topPadding + control.availableHeight / 2 - height / 2
                implicitWidth: 4
                implicitHeight: 12
                radius: 2
                color: control.pressed ? "black" : "#444444"
            }

            onValueChanged: zoomValueChanged(value)
        }

        Item {
            height: 1
            width: 5
        }

        Buttons.ImageButton {
            height: 12
            width: 12
            image.source: "contacts/plus"
            anchors.verticalCenter: parent.verticalCenter

            onClicked: {
                control.increase()
            }
        }

        Item {
            height: 1
            width: 15
        }

        Label {
            id: zoomIndicator
            Layout.alignment: Qt.AlignVCenter|Qt.AlignRight
            Layout.minimumWidth: textMetrics.advanceWidth
            width: textMetrics.advanceWidth
            anchors { verticalCenter: parent.verticalCenter }
            text: Math.round(control.value) + "%"
            font.pointSize: 10
            color: "#444444"
            clip:true
            font.family: openSansRegular.name
            TextMetrics {
                id: textMetrics
                font.family: zoomIndicator.font.family
                text: "100%"
            }
        }

        Item {
            height: 1
            width: 12
        }
    }
}

