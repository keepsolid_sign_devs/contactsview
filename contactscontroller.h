#ifndef CONTACTSCONTROLLER_H
#define CONTACTSCONTROLLER_H

#include <QObject>
#include <QQmlContext>
#include <QWidget>
#include <QQuickWindow>
#include <QQmlApplicationEngine>

#include "contactsortfiltermodel.h"
#include "contactlistmodel.h"
#include "addcontactcontroller.h"

class ContactsController: public QObject
{
    Q_OBJECT

public:
    explicit ContactsController(QObject* _parent = 0);
    ~ContactsController();

private:
    void setContactsModelProperty();
    void registerTypes();
    void setupUI();
    void fillData();
    void setupConnections();

private slots:
    void onAddedContact(ContactModel* contact);
    void onDeleteContact(int index);
    void onEditContact(int index);
    void onFilterContacts(QString name);

private:
    ContactListModel contactsModel;
    ContactSortFilterModel proxyModel;
    QQmlApplicationEngine engine;
    QObject* mainView;

};

#endif // CONTACTSCONTROLLER_H
