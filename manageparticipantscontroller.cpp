#include <QStyle>
#include <QTimer>
#include <QQuickItem>
#include <QApplication>
#include <QDesktopWidget>
#include <QQmlContext>
#include <QCompleter>
#include <QTextEdit>
#include <QDebug>
#include <random>
#include "manageparticipantscontroller.h"

ManageParticipantsController::ManageParticipantsController(QObject* _parent)
    : QObject(_parent)
{
    registerTypes();
    fillData();
    setupUI();
    setupConnections();
}

ManageParticipantsController::ManageParticipantsController(int documentId, QObject* _parent)
    : ManageParticipantsController(_parent)
{
}

ManageParticipantsController::~ManageParticipantsController()
{
    qDebug("ManageParticipantsController destructor");
}

void ManageParticipantsController::registerTypes()
{
    qmlRegisterType<ContactModel>("com.contacts", 1, 0, "ContactModel");
    qmlRegisterType<ContactListModel>("com.contacts", 1, 0, "ContactListModel");
    qmlRegisterType<SuggestionListModel>("com.contacts", 1, 0, "SuggestionListModel");
    qmlRegisterType<SuggestionModel>("com.contacts", 1, 0, "SuggestionModel");
}

void ManageParticipantsController::fillData()
{
    long id = 1;
    observersModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    observersModel.addContact(id++, "JohnJohnJohn", "Doe", "example@gmail.com", "KeepSolidDoe", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    observersModel.addContact(id++, "JohnJohnJohn", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    observersModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    observersModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));

    signersModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    signersModel.addContact(id++, "JohnJohnJohn", "Doe", "example@gmail.com", "KeepSolidDoe", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    signersModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));

    suggestionListModel.addSuggestion(id++, "example@gmail.com 1");
    suggestionListModel.addSuggestion(id++, "bexample@gmail.com 2");
    suggestionListModel.addSuggestion(id++, "exaaample@gmail.com 3");
    suggestionListModel.addSuggestion(id++, "example@gmail.com 4");
    suggestionListModel.addSuggestion(id++, "example@gmail.com 5");
    suggestionListModel.addSuggestion(id++, "example@gmail.com 6");

    setNextStepNecessity(signersModel.rowCount());
}

void ManageParticipantsController::setupUI()
{
    engine.rootContext()->setContextProperty("signersListModel", &signersModel);
    engine.rootContext()->setContextProperty("observersListModel", &observersModel);
    engine.rootContext()->setContextProperty("suggestionListModel", &suggestionListModel);

    QQmlComponent newcomp(&engine, QUrl(QLatin1String("qrc:/contacts/ManageParticipantsPanel.qml")), parent());
    mainView = newcomp.create();
    mainView->setProperty("parent", QVariant::fromValue<QObject*>(parent()));

    QQmlEngine::setObjectOwnership(mainView, QQmlEngine::CppOwnership);
}

void ManageParticipantsController::setupConnections()
{
    QObject* closeBtn = mainView->findChild<QObject*>("closeBtn");
    connect(closeBtn, SIGNAL(closed()), this, SLOT(onClosed()));

    connect(mainView, SIGNAL(signOrderChanged(bool)), this, SLOT(onSignOrderChanged(bool)));

    connect(mainView, SIGNAL(openAddParticipantDialog(int)), this, SLOT(onOpenAddParticipantDialog(int)));
    connect(mainView, SIGNAL(participantRoleChanged(int, int)), this, SLOT(onParticipantRoleChanged(int, int)));
    connect(mainView, SIGNAL(participantPositionChanged(int, int)), this, SLOT(onParticipantPositionChanged(int, int)));
    connect(mainView, SIGNAL(removeParticipant(int, int)), this, SLOT(onRemoveParticipant(int, int)));
    connect(mainView, SIGNAL(addNewContact(QString, int)), this, SLOT(onAddNewContact(QString,int)));
    connect(mainView, SIGNAL(addParticipant(int, int)), this, SLOT(onAddParticipant(int, int)));

    connect(this, SIGNAL(startBusyIndicationFor(QVariant)), mainView, SLOT(startBusyIndicationFor(QVariant)));
    connect(this, SIGNAL(finishBusyIndication()), mainView, SLOT(finishBusyIndication()));
}


void ManageParticipantsController::setNextStepNecessity(bool isNeeded)
{
    if (nextStepNecessity != isNeeded) {
        nextStepNecessity = isNeeded;
        emit nextStepNecessityChanged(nextStepNecessity);
        qDebug("nextStepNecessityChanged on %d", nextStepNecessity==true? 1 : 0);
    }
}

void ManageParticipantsController::onSignOrderChanged(bool isImportant)
{
    qDebug("onSignOrderChanged to %s", (int) isImportant == 1? "true" : "false");
}

void ManageParticipantsController::onParticipantRoleChanged(int contactId, int role)
{
    qDebug("onParticipantRoleChanged with id %d and role %d", contactId, role);
    ContactModel* contact;
    int index = -1;
    if (role == OBSERVER) {
        index = signersModel.indexOf(contactId);
        contact = signersModel.itemAt(index);
        observersModel.addContact(contact);
        signersModel.removeContactAt(index);
    } else {
        index = observersModel.indexOf(contactId);
        contact = observersModel.itemAt(index);
        signersModel.addContact(contact);
        observersModel.removeContactAt(index);
    }
}

void ManageParticipantsController::onParticipantPositionChanged(int contactId, int position)
{
    qDebug("onParticipantPositionChanged with id %d to position %d", contactId, position);
    ContactModel* contact;
    int index = -1;
    index = signersModel.indexOf(contactId);
    signersModel.move(index, position);
}

void ManageParticipantsController::onAddParticipant(int contactId, int role)
{
    qDebug("onAddParticipant with id %d and role %d", contactId, role);
    //get from contact manager, add to participants and get color&status from participant manager. fill model with that data
    SuggestionModel* model = suggestionListModel.itemById(contactId);
    if (model == NULL) {
        model = new SuggestionModel(rand() % 2048, "example@gmail.com");
    }
    ContactModel* contact = new ContactModel(model->suggestionId(), "John", "Doe", model->name(), "Cooompany", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    if (role == SIGNER) {
        signersModel.addContact(contact->contactId(), contact->firstName(), contact->lastName(), contact->email(), contact->company(), contact->iconSource(), contact->color(), contact->status());
    } else {
        observersModel.addContact(contact->contactId(), contact->firstName(), contact->lastName(), contact->email(), contact->company(), contact->iconSource(), contact->color(), contact->status());
    }
    delete contact;
    setNextStepNecessity(signersModel.rowCount());
    QTimer::singleShot(1200, mainView, SLOT(finishBusyIndication()));
}

void ManageParticipantsController::onOpenAddParticipantDialog(int role)
{
    qDebug("onOpenAddParticipantDialog for role %d", role);
}

void ManageParticipantsController::onRemoveParticipant(int contactId, int role)
{
    qDebug("onRemoveParticipant for id %d", contactId);
    if (role == SIGNER) {
        signersModel.removeContactAt(signersModel.indexOf(contactId));
    } else {
        observersModel.removeContactAt(observersModel.indexOf(contactId));
    }
    setNextStepNecessity(signersModel.rowCount());
}

void ManageParticipantsController::onAddNewContact(QString email, int role)
{
    qDebug("onAddNewContact with email " + email.toLatin1() + "");
    //show waiter
    emit startBusyIndicationFor(QVariant::fromValue(role));
    //TODO: send registration request to the server
    //if result is success:
    onAddParticipant(rand(), role);
    //hide waiter
    //    QTimer::singleShot(1200, mainView, SLOT(finishBusyIndication()));

    //else: show error result
}

void ManageParticipantsController::onClosed()
{
    qDebug("onClosed");
}
