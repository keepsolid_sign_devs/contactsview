import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import com.contacts 1.0
import "./buttons" as Buttons

ToolBar {
    id: toolBar
    property DocumentModel document
    property string title: document.name
    property color backgroundColor: "#edecf0"
    property color textColor: "#444444"
    property double backgroundOpacity: 0.7
    property Window attachedWindow;

    signal undoClicked();
    signal redoClicked();
    signal printClicked();
    signal useClicked();

    width: parent.width
    height: 32
    background: Rectangle{
        anchors.fill: parent
        color: backgroundColor
        opacity: backgroundOpacity
    }

    MouseArea {
        id: titleBarMouseRegion
        property var clickPos
        anchors.fill: parent
        onPressed: {
            clickPos = Qt.point(mouseX, mouseY)
        }
        onPositionChanged: {
            if (pressedButtons == Qt.LeftButton) {
                var dx = mouseX - clickPos.x
                var dy = mouseY - clickPos.y
                attachedWindow.x = attachedWindow.x + dx
                attachedWindow.y = attachedWindow.y + dy
                //                clickPos = { x: attachedWindow.x, y: attachedWindow.y }
            }
        }
    }

    RowLayout {
        spacing: 0
        width: parent.width
        height: parent.height

        Item {
            width: 9
            height: 1
        }

        Buttons.ImageButton{
            id: logoImage
            height: parent.height
            width: 32
            anchors { verticalCenter: parent.verticalCenter }
            image.source: "contacts/iconWindowLogo"
            image.height: 14
            image.width: 14
        }

        Buttons.DocumentToolbarButton{
            id: undoBtn
            opacity: document.canUndo()? 1 : 0.5
            enabled: document.canUndo()
            image.source:  "/contacts/iconWindowUndoNormal"
            onClicked:   undoClicked()
        }

        Buttons.DocumentToolbarButton{
            id: redoBtn
            opacity: document.canRedo()? 1 : 0.5
            enabled: document.canRedo()
            image.source:  "/contacts/iconWindowRedoNormal"
            onClicked:   redoClicked()
        }

        Buttons.DocumentToolbarButton{
            id: printBtn
            image.source:  "/contacts/iconWindowPrint"
            onClicked:   printClicked()
        }

        Buttons.DocumentToolbarButton{
            id: useBtn
            visible: true //show only for templates
            image.source:  "/contacts/iconWindowPrint"
            onClicked:   useClicked()
        }

        Label {
            Layout.fillWidth: true
            width: parent.width
            color: textColor
            padding: 10
            text: title
            anchors.verticalCenter: parent.verticalCenter
            font.pointSize: 12
            horizontalAlignment: Qt.AlignHCenter
        }

        Buttons.DocumentToolbarButton {
            id: minimizeBtn
            image.source: "/contacts/iconWindowMinimizeDark"
            onClicked:  attachedWindow.showMinimized()
        }

        Buttons.DocumentToolbarButton {
            id: maximizeBtn
            image.source: "/contacts/iconWindowMaximizeDark"
            onClicked: {
                if (attachedWindow.visibility == Window.Maximized) {
                    attachedWindow.visibility = Window.AutomaticVisibility
                } else {
                    attachedWindow.visibility = Window.Maximized
                }
            }
        }

        Buttons.ImageButton{
            id: closeBtn
            height: parent.height
            width: 44
            anchors { verticalCenter: parent.verticalCenter }
            hoveredColor: "#e7182b"
            hoveredImageSource:  "/contacts/iconWindowCloseLight"
            image.source:  "/contacts/iconWindowCloseDark"
            image.height: 12
            image.width: 12
            onClicked:   attachedWindow.close()
        }
    }
}
