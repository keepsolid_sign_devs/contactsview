#ifndef CONTACTLISTMODEL_H
#define CONTACTLISTMODEL_H

#include <QAbstractListModel>
#include <QList>
#include <memory>

#include "contactmodel.h"

class ContactListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum ContactRoles {
        FirstNameRole = Qt::UserRole,
        LastNameRole = Qt::UserRole + 1,
        EmailRole = Qt::UserRole + 2,
        CompanyRole = Qt::UserRole + 3,
        ColorRole = Qt::UserRole + 4,
        StatusRole = Qt::UserRole + 5,
        IconRole = Qt::UserRole + 6
    };

    ContactListModel(QObject *parent = 0);
    ~ContactListModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    Q_INVOKABLE ContactModel* itemAt(int index) const {
        return dataList.at(index);
    }

    Q_INVOKABLE ContactModel* itemById(int contactId) const {
        for (ContactModel* item: dataList) {
            if (item->contactId() == contactId) {
                return item;
            }
        }
        qDebug("Contact with id %d not found", contactId);
        return NULL;
    }

    Q_INVOKABLE int indexOf(int contactId) const {
        for (int i = 0; i < dataList.count(); i++) {
            ContactModel* item = dataList.at(i);
            if (item->contactId() == contactId) {
                return i;
            }
        }
        qDebug("Contact with id %d not found", contactId);
        return -1;
    }

    Q_INVOKABLE void move(int oldIndex, int newIndex);

    void addContact(ContactModel* contact);
    void addContact(const int& contactId, const QString &firstName, const QString &lastName, const QString &email, const QString &company, const QString &iconSource,
                    const QColor &color);
    void addContact(const int& contactId, const QString &firstName, const QString &lastName, const QString &email, const QString &company, const QString &iconSource,
                    const QColor &color, const int &status);
    bool removeContactAt(const int &index);
    bool editContactAt(const int &index);

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    QList<ContactModel*> dataList;

    friend ContactModel::~ContactModel();
};

#endif // CONTACTLISTMODEL_H
