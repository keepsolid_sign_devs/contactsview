import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import "./buttons" as Buttons

ToolBar {
    id: footer
    property string addButtonText: "Add"
    property alias addButtonVisible: addButton.visible
    signal addButtonClicked
    signal syncButtonClicked

    height: 48

    background: Rectangle {
        anchors.fill: parent
        color: "#f9f9f9"
        Rectangle {
            width: parent.width
            height: 1
            color: "#ebebeb"
            anchors.top: parent.top
        }
    }

    RowLayout {
        anchors.fill: parent
        Item { width: 32 }
        Buttons.SyncButtonFooter {
            id: syncButton
            onClicked: {
                console.log("clicked sync btn!")
                syncButtonClicked()
            }
        }
        Item { Layout.fillWidth: true }
        Buttons.AddButtonFooter {
            id: addButton
            contentText: addButtonText
            onClicked: {
                console.log("clicked add btn!")
                addButtonClicked()
            }
        }
        Item { height: 1; width: 20 }
    }
}
