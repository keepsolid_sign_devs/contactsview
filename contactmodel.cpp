#include <QDebug>
#include "contactmodel.h"

ContactModel::ContactModel(QObject *parent)
    : QObject(parent)
{
}

ContactModel::ContactModel(const int& contactId, const QString &firstName, const QString &lastName, const QString &email,
                           const QString &company, const QString &iconSource, const QColor &color, QObject *parent)
    : QObject(parent), m_contact_id(contactId), m_first_name(firstName), m_last_name(lastName), m_email(email), m_company(company), m_icon_source(iconSource), m_color(color)
{
}

ContactModel::ContactModel(const int& contactId, const QString &firstName, const QString &lastName, const QString &email,
                           const QString &company, const QString &iconSource, const QColor &color, const int& status, QObject *parent)
    : QObject(parent), m_contact_id(contactId), m_first_name(firstName), m_last_name(lastName), m_email(email), m_company(company), m_icon_source(iconSource), m_color(color), m_status(status)
{
}

ContactModel::~ContactModel()
{
    qDebug("Destructor for ContactModel was called");
}

int ContactModel::contactId() const
{
    return m_contact_id;
}

void ContactModel::setContactId(const int &contactId)
{
    if (contactId != m_contact_id) {
        m_contact_id = contactId;
        emit contactIdChanged();
    }
}

QString ContactModel::firstName() const
{
    return m_first_name;
}

void ContactModel::setFirstName(const QString &firstName)
{
    if (firstName != m_first_name) {
        m_first_name = firstName;
        emit firstNameChanged();
    }
}

QString ContactModel::lastName() const
{
    return m_last_name;
}

void ContactModel::setLastName(const QString &lastName)
{
    if (lastName != m_last_name) {
        m_last_name = lastName;
        emit lastNameChanged();
    }
}

QString ContactModel::email() const
{
    return m_email;
}

void ContactModel::setEmail(const QString &email)
{
    if (email != m_email) {
        m_email = email;
        emit emailChanged();
    }
}

QString ContactModel::company() const
{
    return m_company;
}

void ContactModel::setCompany(const QString &company)
{
    if (company != m_company) {
        m_company = company;
        emit companyChanged();
    }
}

QString ContactModel::iconSource() const
{
    return m_icon_source;
}

void ContactModel::setIconSource(const QString &iconSource)
{
    if (iconSource != m_icon_source) {
        m_icon_source = iconSource;
        emit iconSourceChanged();
    }
}

QColor ContactModel::color() const
{
    return m_color;
}

void ContactModel::setColor(const QColor &color)
{
    if (color != m_color) {
        m_color = color;
        emit colorChanged();
    }
}

int ContactModel::status() const
{
    return m_status;
}

void ContactModel::setStatus(const int &status)
{
    if (status != m_status) {
        m_status = m_status;
        emit statusChanged();
    }
}
