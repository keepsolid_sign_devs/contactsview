#ifndef SUGGESTIONMODEL_H
#define SUGGESTIONMODEL_H

#include <QObject>

class SuggestionModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int suggestionId READ suggestionId WRITE setSuggestionId NOTIFY suggestionIdChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)

public:
    SuggestionModel(QObject *parent=0);
    SuggestionModel(const int& suggestionId, const QString &name, QObject *parent=0);
    ~SuggestionModel();

    int suggestionId() const;
    QString name() const;

    void setSuggestionId(const int &suggestionId);
    void setName(const QString &name);

signals:
    void suggestionIdChanged();
    void nameChanged();

private:
    int m_suggestion_id;
    QString m_name;
};

#endif // SUGGESTIONMODEL_H
