import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import com.contacts 1.0
import "../texts" as TextViews
import "../buttons" as Buttons
import "../styles"

Item {
    id: root
    objectName: "root"
    width: 300
    height: 250

    function startBusyIndication() {
        busyIndicator.running = true
    }

    function finishBusyIndication() {
        busyIndicator.running = false
    }

    FontLoader {
        id: openSansRegular
        source: "OpenSansFonts/OpenSans-Regular.ttf"
    }

    ToolBar {
        id: toolBar
        width: parent.width
        height: 32
        background: Rectangle {
            anchors.fill: parent
            color: "#2980cc"
        }
        Label {
            width: parent.width
            color: "white"
            padding: 10
            text: qsTr("Add contact")
            anchors.verticalCenter: parent.verticalCenter
            font.pointSize: 12
            font.family: openSansRegular.name
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            id: closeBtn
            objectName: "closeBtn"
            signal closed()
            width: 20
            height: 20
            anchors { right: parent.right; rightMargin: 15; verticalCenter: parent.verticalCenter }
            Image {
                id: closeImg
                width: parent.width
                height: parent.height
                sourceSize.width: width
                sourceSize.height: height
                antialiasing: true
                fillMode: Image.PreserveAspectFit
                source: "http://www.5pointwellness.com/images/closebutton.png"
            }
            // Respond to the signal here.
            onClicked: { closed() }
        }
    }

    ColumnLayout {
        id: fieldsLayout
        anchors { top: toolBar.bottom; bottom: parent.bottom }
        width: parent.width
        spacing: 10

        Item {
            width: 1
            height: 10
        }

        TextViews.InputField {
            id: emailField
            objectName: "emailField"
            width: parent.width * 0.85
            hintText: "Email"
            actionBtnSize: 14
            actionBtnSource: "http://www.iconsdb.com/icons/download/tropical-blue/plus-4-128.png"
            anchors { horizontalCenter: parent.horizontalCenter }
        }

        TextViews.InputField {
            id: nameField
            width: parent.width * 0.85
            hintText: "Name"
            anchors { horizontalCenter: parent.horizontalCenter }
        }

        TextViews.InputField {
            id: surnameField
            width: parent.width * 0.85
            hintText: "Surname"
            anchors { horizontalCenter: parent.horizontalCenter }
        }

        Item {
            width: 1
            Layout.fillHeight: true
        }

        Buttons.StateButton {
            id: addBtn
            objectName: "addBtn"
            anchors { right: parent.right; rightMargin: 20 }
            signal addContact(ContactModel contact)
            stateStyle: ButtonTypes.primary
            contentText: "Add"
            onClicked: {
                if (emailField.text=="") {
                    emailField.warningText = "Something is wrong"
                } else {
                    var contact = contactComponent.createObject(addBtn)
                    contact.contactId = Math.random()
                    contact.firstName = nameField.text
                    contact.lastName = surnameField.text
                    contact.email = emailField.text
                    contact.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                    addContact(contact)
                }
            }
        }

        Item {
            width: 1
            height: 10
        }

        Component {
            id: contactComponent
            ContactModel {}
        }
    }

    BusyIndicator {
        id: busyIndicator
        objectName: "busyIndicator"
        height: parent.height
        width: parent.width
        focusPolicy: Qt.WheelFocus
        background: Rectangle {
            color: "black"
            opacity: 0.2
        }
        padding: width * 0.3
        focus: running
        visible: running
        running: false
    }
}
