import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import com.contacts 1.0
import QtQml.Models 2.2
import "./../buttons" as Buttons


ItemDelegate {
    property alias colorIndication: colorIndicator.visible
    property alias dropArea: dropArea
    property alias actionButton: actionBtn
    property bool dividerVisible: true
    property bool draggable: false
    property bool statusIndication: false

    id: wrapper
    width: listView.width - listView.leftMargin - listView.rightMargin
    height: statusIndication? 65 : 55

    FontLoader {
        id: openSansRegular
        source: "../OpenSansFonts/OpenSans-Regular.ttf"
    }

    background: Rectangle {
        anchors.fill: parent
        anchors { topMargin: 5; bottomMargin: 5 }
        color: hovered?
                   draggable || !clickable? "white" : "#f6fbff"
        : "transparent"
        opacity: 0.75
    }

    Rectangle {
        id: divider
        visible: dividerVisible
        width: parent.width
        height: 1
        color: "#ebebeb"
        anchors.bottom: parent.bottom
    }

    states:  [
        State {
            name: "Default"
            when: !wrapper.ListView.isCurrentItem
            PropertyChanges { target: wrapperTitle; color: "#5e5e5e"}
            PropertyChanges { target: wrapperSubtitle; color: "black"; opacity: 0.5 }
        },
        State {
            name: "Current"
            when: wrapper.ListView.isCurrentItem
            PropertyChanges { target: wrapperTitle; color: "#5e5e5e"}
            PropertyChanges { target: wrapperSubtitle; color: "black"; opacity: 0.5 }
        }
    ]

    MouseArea {
        id: mouseArea
        visible: draggable
        anchors.fill: parent
        drag.target: content
        drag.axis: Drag.YAxis

        onPositionChanged: {
            dragPositionChanged(content.y)
        }

        drag.onActiveChanged: {
            console.log("onActiveChanged")
            if (mouseArea.drag.active) {
                dragStarted()
                listView.dragItemIndex = index;
            } else {
                if(dropArea.containsDrag) {
                    console.log("drop.containsDrag")
                    dragFinished()
                } else {
                    dragFinished()
                    visualModel.model.modelReset()
                }
                content.Drag.drop();
            }
        }
        DropArea {
            id: dropArea
            anchors { fill: parent; /*margins: 10 */}

            onDropped: {
                var from = index
                var to = drag.source.DelegateModel.itemsIndex
                var currentItem = visualModel.model.itemAt(from)
                console.log("onDropped " + from + "->" + to)
                console.log("onDropped contact email " + currentItem.email + ", id " + currentItem.contactId)
                if (from !== to) {
                    contactPositionChanged(currentItem.contactId, to)
                } else {
                    visualModel.model.modelReset()
                }
            }

            onEntered: {
                var from = drag.source.DelegateModel.itemsIndex
                var to = wrapper.DelegateModel.itemsIndex
                console.log("onEntered " + from + "->" + to)
                visualModel.items.move(from, to)
            }
        }
    }

    Item {
        id: content
        width: parent.width
        height: parent.height

        states: [
            State {
                when: content.Drag.active
                ParentChange {
                    target: content
                    parent: listView.parent.parent
                }

                AnchorChanges {
                    target: content
                    anchors.horizontalCenter: undefined
                    anchors.verticalCenter: undefined
                }

                PropertyChanges {
                    target: content
                    z: 200
                }
            }
        ]

        Drag.active: mouseArea.drag.active
        Drag.source: wrapper
        Drag.hotSpot.x: content.width / 2
        Drag.hotSpot.y: content.height / 2

        Rectangle {
            visible: draggable
            width: parent.width; height: parent.height
            color: mouseArea.drag.active? "#daebf9" : "transparent"
            border.color: mouseArea.drag.active? "#332980cc" : "transparent"
            border.width: 1
            //            anchors { top: parent.top; topMargin: 5}
            //            y: listView.currentItem.y + 5
        }

        RowLayout {
            width: parent.width
            height: parent.height
            spacing: 0

            Item { //kostil for proper spacing
                height: 1
                width: 4
            }

            Rectangle {
                id: wrapperIcon
                width: 30
                height: 30
                radius: width / 2
                antialiasing: true
                color: "white"

                Image {
                    width: model.iconSource? 28 : 30
                    height: model.iconSource? 28 : 30
                    sourceSize.width: width
                    sourceSize.height: height
                    antialiasing: true
                    fillMode: Image.PreserveAspectFit
                    anchors { centerIn: wrapperIcon }
                    source: model.iconSource? model.iconSource : "../contacts/default_contact_icon_small"
                }
                Rectangle {
                    id: colorIndicator
                    width: 8
                    height: width
                    radius: width / 2
                    border.color: "white"
                    border.width: 1
                    color: model.color
                    anchors { left: parent.left; bottom: parent.bottom }
                }
            }

            Item { //kostil for proper spacing
                height: 1
                width: 11
            }

            ColumnLayout {
                id: contactColumn
                Layout.fillWidth: true
                Layout.fillHeight: true
                anchors { verticalCenter: parent.verticalCenter;}
                property bool hasName: model.firstName || model.lastName
                spacing: 0

                Item {
                    Layout.fillHeight: true
                }

                Text {
                    id: wrapperTitle
                    Layout.fillWidth: true
                    text: (considerOrder? index+1 + ". " : "") + (contactColumn.hasName? model.firstName + " " + model.lastName : model.email)
                    leftPadding: 3
                    clip: true
                    maximumLineCount: 1
                    verticalAlignment: contactColumn.hasName? Text.AlignBottom : Text.AlignVCenter
                    elide: Text.ElideRight
                    wrapMode: Text.WrapAnywhere
                    textFormat: Text.PlainText
                    font.pointSize: 12
                    font.family: openSansRegular.name
                    Behavior on color {
                        ColorAnimation { target: wrapperTitle; duration: 300 }
                    }
                }
                Text {
                    id: wrapperSubtitle
                    text: model.email
                    Layout.fillWidth: true
                    visible: contactColumn.hasName
                    leftPadding: 3
                    clip: true
                    maximumLineCount: 1
                    verticalAlignment: Text.AlignTop
                    elide: Text.ElideRight
                    wrapMode: Text.WrapAnywhere
                    textFormat: Text.PlainText
                    font.pointSize: 9
                    font.family: openSansRegular.name
                    Behavior on color {
                        ColorAnimation { target: wrapperSubtitle; duration: 300 }
                    }
                }
                Text {
                    id: wrapperAdditionalSubtitle
                    Layout.fillWidth: true
                    leftPadding: 3
                    visible: statusIndication
                    clip: true
                    maximumLineCount: 1
                    verticalAlignment: Text.AlignTop
                    elide: Text.ElideRight
                    wrapMode: Text.WrapAnywhere
                    textFormat: Text.PlainText
                    font.pointSize: 9
                    font.family: openSansRegular.name
                    Behavior on color {
                        ColorAnimation { target: wrapperSubtitle; duration: 300 }
                    }
                    text: {
                        var status = model.status
                        var text = status===0||status===1? "Unread" :
                                    status === 2? "Waiting for queue" :
                                    status === 3? "Waiting" :
                                    "Complete"
                        text;
                    }
                    color: {
                        var status = model.status
                        var color = status===0||status===1? "#2980cc" : //need color for unread
                                    status===2||status===3? "#ffcc00" :
                                    "#8ddc75"
                        color;
                    }
                }
                Item {
                    Layout.fillHeight: true
                }
            }
        }
    }

    MouseArea {
        visible: !draggable && clickable
        anchors.fill: parent
        onClicked: { listView.currentIndex = index; itemClicked() }
    }

    Buttons.ImageButton {
        id: actionBtn
        width: 14
        height: parent.height
        anchors { right: parent.right; rightMargin: 4 }
        image.width: 12
        image.height: 12
        image.source: actionBtnSource
        visible: false

        onClicked: {
//            onActionBtnClicked: {
                var currentItem = contactsModel.itemAt(index)
                actionBtnClicked(currentItem.contactId)
//            }
        }
    }
}
