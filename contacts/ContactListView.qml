import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import QtQml.Models 2.2
import com.contacts 1.0
import "." as Contacts

Item {
    //    property ContactSortFilterModel contactsModel
    property ContactListModel contactsModel
    property ListView list
    property int scrollBarWidth: 8
    property bool colorIndication: false
    property bool statusIndication: false
    property bool dividerVisible: true
    property bool innerDragDrop: true
    property bool considerOrder: false
    property bool actionBtnVisibility: false
    property bool draggable: false
    property bool clickable: true
    property string actionBtnSource
    property alias visualModel: visualModel
    property alias dummyText: dummyText.text
    property alias dummyList: dummyList

    signal contactPositionChanged(int contactId, int position)
    signal actionBtnClicked(int contactId)

    signal dragPositionChanged(int y)
    signal dragStarted()
    signal dragFinished()
    signal itemClicked()

    FontLoader {
        id: openSansRegular
        source: "../OpenSansFonts/OpenSans-Regular.ttf"
    }

    id: rootLayout
    width: parent.width
    height: parent.height

    Item {
        id: dummyList
        visible: list.count < 1
        height: 55
        width: parent.width
        anchors { top: parent.top; topMargin: 7 }
        Image {
            id: dummyIcon
            anchors { right: parent.right; top: parent.top; rightMargin: 15 }
            width: 19
            height: width
            sourceSize.width: width
            sourceSize.height: width
            source: "top_right_arrow"
        }

        Text {
            id: dummyText
            Layout.alignment: Qt.AlignLeft
            anchors { right: dummyIcon.left; left: parent.left; rightMargin: 4; top: dummyIcon.verticalCenter }

            Layout.fillWidth: true
            maximumLineCount: 3
            elide: Text.ElideRight
            wrapMode: Text.WordWrap
            textFormat: Text.PlainText
            clip: true
            color: "#444444"
            font.pointSize: 10
            font.family: openSansRegular.name
        }

    }

    list: ListView {
        property int dragItemIndex: -1

        id: listView
        clip: true
        focus: true
        activeFocusOnTab: true
        parent: rootLayout
        visible: list.count > 0
        width: draggable || !clickable? parent.width : parent.width - (scrollBar.width + scrollBar.anchors.leftMargin)
        height: parent.height
        interactive: !draggable && clickable
        Component.onCompleted:{
            listView.forceActiveFocus()
        }
//                snapMode: ListView.SnapOneItem
        spacing: 0
        model: visualModel
        highlight: highlight
        highlightFollowsCurrentItem: false
        highlightMoveVelocity: 1000
        onCurrentIndexChanged: {
            forceActiveFocus()
            //                        focus = true //TAKAYA DICH
            console.log(activeFocus)
        }
        ScrollBar.vertical: ScrollBar {
            id:scrollBar
            parent: listView.parent
            hoverEnabled: true
            active: hovered
            width: scrollBarWidth
            contentItem: Rectangle {
                color: scrollBar.pressed ? "#2678c0" : "#2980cc"
            }
            background: Rectangle {
                color: "#ebebeb"
            }
            anchors.top: listView.top
            anchors.left: listView.right
            anchors.bottom: listView.bottom
            anchors.leftMargin: 10
        }
    }

    DelegateModel {
        id: visualModel

        model: contactsModel
        delegate: Component {
            Contacts.ContactDelegate {
                id: contactDelegate
                dropArea.visible: rootLayout.innerDragDrop && considerOrder
                colorIndication: rootLayout.colorIndication
                statusIndication: rootLayout.statusIndication
                dividerVisible: rootLayout.dividerVisible
                actionButton.visible: actionBtnVisibility && (typeof(documentModel)!=="undefined"? !documentModel.editable && documentModel.canNotify(contactsModel.itemAt(index).contactId) : true)
                draggable: rootLayout.draggable
            }
        }
    }

    Component {
        id: highlight
        Rectangle {
            width: parent.width; height: parent.height / listView.count - 11
            color: "#daebf9"
            border.color: "#332980cc"
            border.width: 1
            y: listView.currentItem.y + 5
            Behavior on y {
                SpringAnimation {
                    spring: 2
                    damping: 0.2
                }
            }
        }
    }
}
