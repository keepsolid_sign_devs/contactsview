import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import com.contacts 1.0
import "../texts" as TextViews
import "../buttons" as Buttons
import "."

Item {
    id: panel
    objectName: "panel"

    readonly property int signerRole: 0
    readonly property int observerRole: 1

    signal signOrderChanged(bool isImportant)
    signal participantRoleChanged(int contactId, int role)
    signal participantPositionChanged(int contactId, int role)
    signal removeParticipant(int contactId, int role)
    signal openAddParticipantDialog(int role)
    signal addParticipant(int contactId, int role)
    signal addNewContact(string email, int role)

    Layout.maximumWidth: 400
    Layout.minimumWidth: 254

    height: parent? parent.height : 0
    width: parent? parent.width : 0

    FontLoader {
        id: openSansRegular
        source: "../OpenSansFonts/OpenSans-Regular.ttf"
    }

    function startBusyIndicationFor(role) {
        if (role === signerRole) {
            signersInputLayout.inputField.busy = true
        } else {
            observersInputLayout.inputField.busy = true
        }
    }

    function finishBusyIndication() {
        signersInputLayout.inputField.text=""
        observersInputLayout.inputField.text=""
        signersInputLayout.inputField.busy = false
        observersInputLayout.inputField.busy = false
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            console.log("forceActiveFocus flickable");
            flickable.forceActiveFocus();
        }
    }

    Rectangle {
        anchors.fill: parent
        color: "#f9f9f9"
    }

    ToolBar {
        id: toolBar
        width: parent.width
        height: 32
        anchors { left: parent.left; leftMargin: 24; right: parent.right; rightMargin: 15 }
        anchors { top: parent.top; topMargin: 20 }
        background: Rectangle {
            anchors.fill: parent
            color: "transparent"
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                toolBar.forceActiveFocus();
            }
        }
        Label {
            width: parent.width
            color: "#444444"
            text: "Manage participants"
            anchors { verticalCenter: parent.verticalCenter; right: closeBtn.left;  left: parent.left}
            font.pointSize: 18
            clip: true
            rightPadding: 6
            maximumLineCount: 1
            elide: Text.ElideRight
            //            textFormat: Text.PlainText
            font.family: openSansRegular.name
            font.weight: Font.Light
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            id: closeBtn
            objectName: "closeBtn"
            signal closed()
            width: 10
            height: 10
            anchors { verticalCenter: parent.verticalCenter; right: parent.right }
            Image {
                id: closeImg
                width: parent.width
                height: parent.height
                sourceSize.width: width
                sourceSize.height: height
                antialiasing: true
                fillMode: Image.PreserveAspectFit
                source: "iconWindowCloseDark"
            }
            // Respond to the signal here.
            onClicked: {
                closed()
            }
        }
    }

    Flickable {
        property int scrollStep: signersListView.list.currentItem? signersListView.list.currentItem.height : 55

        id: flickable
        width: parent.width
        height: parent.height
        anchors { left: parent.left; leftMargin: 24; right: parent.right; rightMargin: 15;
            top: toolBar.bottom; topMargin: 20; bottom: parent.bottom }
        Layout.fillHeight: true
        contentHeight: scrollLayout.height
        flickableChildren: scrollLayout
        flickableDirection: Flickable.VerticalFlick
        boundsBehavior: Flickable.StopAtBounds
        clip: true

        Component.onCompleted:{
            flickable.forceActiveFocus()
        }

        Keys.onUpPressed: {
            console.log("up")
            scrollUp();
        }

        Keys.onDownPressed: {
            console.log("down")
            scrollDown();
        }

        function scrollToPosition(y) {
            if (y === 0) {
                return
            }
            var scrollStep = y + 30 - (flickable.contentY + flickable.height)
            if (scrollStep > 0) { //down if needed
                flickable.contentY += scrollStep
                if (flickable.contentY > flickable.contentHeight - panel.height + flickable.scrollStep) {
                    flickable.contentY = flickable.contentHeight - panel.height + flickable.scrollStep
                }
            } else if ((scrollStep = flickable.contentY - (y + 30)) > 0) { //up if needed
                flickable.contentY -= scrollStep
                if (flickable.contentY < 0) {
                    flickable.contentY = 0
                }
            }
        }

        function scrollUp() {
            if (flickable.contentY > 0) {
                flickable.contentY -= scrollStep
            }
            if (flickable.contentY < 0) {
                flickable.contentY = 0
            }
        }

        function scrollDown() {
            var oopsMagicNumber = 65
            if (flickable.contentY < flickable.contentHeight - panel.height + oopsMagicNumber) {
                flickable.contentY += scrollStep
            }
            if (flickable.contentY > flickable.contentHeight - panel.height + oopsMagicNumber) {
                flickable.contentY = flickable.contentHeight - panel.height + oopsMagicNumber
            }
        }

        ScrollBar.vertical: ScrollBar {
            id: scrollBar
            Layout.fillHeight: true
            parent: flickable.parent
            hoverEnabled: true
            active: hovered
            clip: false
            width: 8
            size: 0.3
            contentItem: Rectangle {
                width: parent.width
                height: flickable.contentHeight
                color: scrollBar.pressed ? "#2678c0" : "#2980cc"
            }
            background: Rectangle {
                color: "#ebebeb"
            }
            anchors.top: flickable.top
            anchors.left: flickable.right
            anchors.bottom: flickable.bottom
            anchors.leftMargin: 8
        }

        ColumnLayout {
            id: scrollLayout
            spacing: 0
            Layout.fillWidth: true
            Layout.fillHeight: true
            width: parent.width

            Label {
                id: signersHeader
                Layout.minimumWidth: implicitWidth
                Layout.alignment: Qt.AlignLeft
                text: "Signers:"
                font.pointSize: 14
                color: "#2980cc"
                clip:true
                font.family: openSansRegular.name
                font.weight: Font.Bold
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        flickable.forceActiveFocus();
                    }
                }
            }

            Item {
                height: 11
                width: 1
            }

            TextViews.InputFieldWithSuggestions {
                id: signersInputLayout
                objectName: "signersInputLayout"
                width: parent.width
                Layout.fillWidth: true
                inputField.hintText: "Invite signer"
                onActionBtnClicked: { openAddParticipantDialog(signerRole); }
                inputField.actionBtnSize: 14
                inputField.actionBtnSource: "http://www.iconsdb.com/icons/download/tropical-blue/plus-4-128.png"
                z: 100
                suggestionsModel: suggestionListModel
                onSuggestionChosen: {
                    addParticipant(item.suggestionId, signerRole)
                }
                onUserInputChosen: {
                    var email = inputField.text.trim()
                    if (email == "") {
                        inputField.warningText = "Invalid email!"
                        return
                    }
                    addNewContact(email, signerRole)
                }
            }

            Buttons.KSCheckBox {
                id: signOrderCheckbox
                visible: signersListView.list.count > 0
                Layout.fillWidth: true
                text: "Sign order is important"
                font.pointSize: 8
                onCheckedChanged: {
                    forceActiveFocus()
                    signOrderChanged(checked)
                }
            }

            ContactListView {
                id: signersListView
                height: list.count > 0? list.contentHeight : dummyList.height
                Layout.preferredHeight: height
                Layout.fillWidth: true
                colorIndication: true
                draggable: true
                dividerVisible: false
                list.interactive: false
                list.currentIndex: -1
                considerOrder: signOrderCheckbox.checked
                actionBtnVisibility: true
                actionBtnSource: "ic_trash_grey"
                dummyText: "No signers. You can add them, click"

                onDragPositionChanged: {
                    flickable.scrollToPosition(y)
                }

                onActionBtnClicked: {
                    removeParticipant(contactId, signerRole)
                }
                //                list.snapMode: ListView.SnapOneItem
                list.onCurrentIndexChanged: {
                    var currIndex = list.currentIndex
                    if (!contactsModel.hasIndex(currIndex, 0)) {
                        return
                    }
                    var currItem = contactsModel.itemAt(currIndex);
                    //                    chooseSigner(currItem.contactId)

                }
                onContactPositionChanged: {
                    participantPositionChanged(contactId, position)
                }

                onDragStarted: {
                    console.log("onDragStarted")
                    flickable.forceActiveFocus()
                    signersListView.innerDragDrop = true
                }

                onDragFinished: {
                    console.log("onDragFinished")
                    signersListView.innerDragDrop = false
                }
                contactsModel: signersListModel
                DropArea {
                    id: signersDropArea
                    anchors.fill: parent
                    Rectangle {
                        id: signersDropAreaBackground
                        anchors.fill: parent
                        color: "transparent"
                    }
                    onDropped: {
                        console.log("onDropped")
                        var index = observersListView.list.dragItemIndex;
                        if (index >= 0 && index < observersListView.list.count) {
                            var currentItem = observersListModel.itemAt(observersListView.list.dragItemIndex)
                            participantRoleChanged(currentItem.contactId, signerRole)
                            observersListView.list.dragItemIndex = -1
                        }
                    }
                    states: [
                        State {
                            when: signersDropArea.containsDrag && observersListView.list.dragItemIndex!==-1
                            PropertyChanges {
                                target: signersDropAreaBackground
                                color: "#daebf9"
                            }
                        }
                    ]
                }
            }

            Item {
                id: divider1
                height: 24
                width: 1
            }

            Label {
                id: observersHeader
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        flickable.forceActiveFocus();
                    }
                }
                Layout.minimumWidth: implicitWidth
                Layout.alignment: Qt.AlignLeft
                text: "Observers:"
                font.pointSize: 14
                color: "#2980cc"
                clip:true
                font.family: openSansRegular.name
                font.weight: Font.Bold
            }

            Item {
                id: divider2
                height: 11
                width: 1
            }

            TextViews.InputFieldWithSuggestions {
                id: observersInputLayout
                objectName: "observersInputLayout"
                Layout.fillWidth: true
                inputField.hintText: "Invite observer"
                inputField.actionBtnSize: 14
                inputField.actionBtnSource: "http://www.iconsdb.com/icons/download/tropical-blue/plus-4-128.png"
                z: 100
                onActionBtnClicked: openAddParticipantDialog(observerRole)
                suggestionsModel: suggestionListModel
                onSuggestionChosen: {
                    addParticipant(item.suggestionId, observerRole)
                }
                onUserInputChosen: {
                    var email = inputField.text.trim()
                    if (email == "") {
                        inputField.warningText = "Invalid email!"
                        return
                    }
                    addNewContact(email, observerRole)
                }
            }

            Item {
                id: divider3
                visible: observersListView.list.count > 0
                height: 10
                width: 1
            }

            ContactListView {
                id: observersListView
                height: list.contentHeight
                Layout.preferredHeight: height
                Layout.fillWidth: true
                colorIndication: true
                dividerVisible: false
                list.interactive: false
                list.currentIndex: -1
                innerDragDrop: false
                draggable: true
                actionBtnVisibility: true
                actionBtnSource: "ic_trash_grey"
                dummyText: "No observers. You can add them, click"
                onActionBtnClicked: {
                    removeParticipant(contactId, observerRole)
                }

                onDragPositionChanged: {
                    flickable.scrollToPosition(y)
                }

                onDragStarted: {
                    console.log("onDragStarted")
                    flickable.forceActiveFocus()
                    signersListView.innerDragDrop = false
                }

                onDragFinished: {
                    console.log("onDragFinished")
                    signersListView.innerDragDrop = true
                }
                onContactPositionChanged: {
                    participantPositionChanged(contactId, position)
                }

                list.onCurrentIndexChanged: {
                    var currIndex = list.currentIndex
                    if (!contactsModel.hasIndex(currIndex, 0)) {
                        return
                    }
                    var currItem = contactsModel.itemAt(currIndex);
                    //                    chooseSigner(currItem.contactId)

                }
                contactsModel: observersListModel

                DropArea {
                    id: observersDropArea
                    anchors.fill: parent
                    Rectangle {
                        id: observersDropAreaBackground
                        anchors.fill: parent
                        color: "transparent"
                    }
                    onDropped: {
                        console.log("onDropped")
                        if (signersListView.list.dragItemIndex !== -1) {
                            var currentItem = signersListModel.itemAt(signersListView.list.dragItemIndex)
                            participantRoleChanged(currentItem.contactId, observerRole)
                            signersListView.list.dragItemIndex = -1
                        }
                    }
                    states: [
                        State {
                            when: observersDropArea.containsDrag && signersListView.list.dragItemIndex!==-1
                            PropertyChanges {
                                target: observersDropAreaBackground
                                color: "#daebf9"
                            }
                        }
                    ]
                }
            }
        }
    }
}
