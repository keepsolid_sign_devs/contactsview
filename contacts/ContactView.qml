import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import com.contacts 1.0
import QtQml.Models 2.2
import "./../buttons" as Buttons


ItemDelegate {
    property alias colorIndication: colorIndicator.visible
    property alias actionButton: actionBtn
    property bool dividerVisible: true
    property bool statusIndication: false
    property ContactModel model: undefined
    property string actionBtnSource

    id: wrapper
    height: statusIndication? 65 : 55

    FontLoader {
        id: openSansRegular
        source: "../OpenSansFonts/OpenSans-Regular.ttf"
    }

    background: Rectangle {
        anchors.fill: parent
        anchors { topMargin: 5; bottomMargin: 5 }
        color: hovered? "#f6fbff" : "transparent"
        opacity: 0.75
    }

    Item {
        id: content
        width: parent.width
        height: parent.height

        RowLayout {
            width: parent.width
            height: parent.height
            spacing: 0

            Item { //kostil for proper spacing
                height: 1
                width: 4
            }

            Rectangle {
                id: wrapperIcon
                width: 30
                height: 30
                radius: width / 2
                antialiasing: true
                color: "white"
                anchors.verticalCenter: parent.verticalCenter

                Image {
                    width: model.iconSource? 28 : 30
                    height: model.iconSource? 28 : 30
                    sourceSize.width: width
                    sourceSize.height: height
                    antialiasing: true
                    fillMode: Image.PreserveAspectFit
                    anchors { centerIn: wrapperIcon }
                    source: model.iconSource? model.iconSource : "../contacts/default_contact_icon_small"
                }
                Rectangle {
                    id: colorIndicator
                    width: 8
                    height: width
                    radius: width / 2
                    border.color: "white"
                    border.width: 1
                    color: model.color
                    anchors { left: parent.left; bottom: parent.bottom }
                }
            }

            Item { //kostil for proper spacing
                height: 1
                width: 11
            }

            ColumnLayout {
                id: contactColumn
                Layout.fillWidth: true
                Layout.fillHeight: true
                anchors { verticalCenter: parent.verticalCenter;}
                property bool hasName: model.firstName || model.lastName
                spacing: 0

                Item {
                    Layout.fillHeight: true
                }

                Text {
                    id: wrapperTitle
                    Layout.fillWidth: true
                    text: contactColumn.hasName? model.firstName + " " + model.lastName : model.email
                    leftPadding: 3
                    clip: true
                    maximumLineCount: 1
                    verticalAlignment: contactColumn.hasName? Text.AlignBottom : Text.AlignVCenter
                    elide: Text.ElideRight
                    wrapMode: Text.WrapAnywhere
                    textFormat: Text.PlainText
                    font.pointSize: 12
                    font.family: openSansRegular.name
                    color: "#5e5e5e"
                    Behavior on color {
                        ColorAnimation { target: wrapperTitle; duration: 300 }
                    }
                }
                Text {
                    id: wrapperSubtitle
                    text: model.email
                    Layout.fillWidth: true
                    visible: contactColumn.hasName
                    leftPadding: 3
                    clip: true
                    color: "black"
                    opacity: 0.5
                    maximumLineCount: 1
                    verticalAlignment: Text.AlignTop
                    elide: Text.ElideRight
                    wrapMode: Text.WrapAnywhere
                    textFormat: Text.PlainText
                    font.pointSize: 9
                    font.family: openSansRegular.name
                    Behavior on color {
                        ColorAnimation { target: wrapperSubtitle; duration: 300 }
                    }
                }
                Text {
                    id: wrapperAdditionalSubtitle
                    Layout.fillWidth: true
                    leftPadding: 3
                    visible: statusIndication
                    clip: true
                    maximumLineCount: 1
                    verticalAlignment: Text.AlignTop
                    elide: Text.ElideRight
                    wrapMode: Text.WrapAnywhere
                    textFormat: Text.PlainText
                    font.pointSize: 9
                    font.family: openSansRegular.name
                    Behavior on color {
                        ColorAnimation { target: wrapperSubtitle; duration: 300 }
                    }
                    text: {
                        var status = model.status
                        var text = status===0||status===1? "Unread" :
                                    status === 2? "Waiting for queue" :
                                    status === 3? "Waiting" :
                                    "Complete"
                        text;
                    }
                    color: {
                        var status = model.status
                        var color = status===0||status===1? "#2980cc" : //need color for unread
                                    status===2||status===3? "#ffcc00" :
                                    "#8ddc75"
                        color;
                    }
                }
                Item {
                    Layout.fillHeight: true
                }
            }
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: { listView.currentIndex = index; itemClicked() }
    }

    Buttons.ImageButton {
        id: actionBtn
        width: 14
        height: parent.height
        anchors { right: parent.right; rightMargin: 4 }
        image.width: 12
        image.height: 12
        image.source: actionBtnSource
        visible: false

        onClicked: {
            console.log("actionBtn clicked!")
        }
    }
}
