import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import "./../texts" as TextViews

Item {
    id: panel
    objectName: "infoPanel"

    readonly property int signerRole: 0
    readonly property int observerRole: 1

    property bool userIsAuthor: false

    Layout.maximumWidth: 650
    Layout.minimumWidth: 300

    height: parent? parent.height : 0
    width: parent? parent.width : 0

    FontLoader {
        id: openSansRegular
        source: "../OpenSansFonts/OpenSans-Regular.ttf"
    }

    FontLoader {
        id: openSansLight
        source: "../OpenSansFonts/OpenSans-Light.ttf"
    }

    FontLoader {
        id: openSansSemibold
        source: "../OpenSansFonts/OpenSans-Semibold.ttf"
    }

    Rectangle {
        anchors.fill: parent
        color: "#f9f9f9"
    }

    ToolBar {
        id: toolBar
        width: parent.width * 0.8
        height: 32
        anchors { top: parent.top; topMargin: 20; horizontalCenter: parent.horizontalCenter }
        background: Rectangle {
            anchors.fill: parent
            color: "transparent"
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                toolBar.forceActiveFocus();
            }
        }
        Label {
            width: parent.width
            color: "#444444"
            text: "Document info"
            anchors { verticalCenter: parent.verticalCenter; right: closeBtn.left;  left: parent.left}
            font.pointSize: 18
            clip: true
            rightPadding: 6
            maximumLineCount: 1
            elide: Text.ElideRight
            //            textFormat: Text.PlainText
            font.family: openSansLight.name
            font.weight: Font.Light
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            id: closeBtn
            objectName: "closeBtn"
            signal closed()
            width: 10
            height: 10
            anchors { verticalCenter: parent.verticalCenter; right: parent.right }
            Image {
                id: closeImg
                width: parent.width
                height: parent.height
                sourceSize.width: width
                sourceSize.height: height
                antialiasing: true
                fillMode: Image.PreserveAspectFit
                source: "iconWindowCloseDark"
            }
            // Respond to the signal here.
            onClicked: {
                closed()
            }
        }
    }

    GridLayout {
        id: mainInfoLayout
        width: parent.width * 0.8
        columns: 2
        rows:2
        rowSpacing: 20
        columnSpacing: 20
        anchors { top: toolBar.bottom; topMargin: 20; horizontalCenter: parent.horizontalCenter }
        Layout.alignment: Qt.AlignHCenter

        Label {
            id: creationDateLabel
            Layout.alignment: Qt.AlignLeft|Qt.AlignTop
            topPadding: 4
            text: "Creation date:"
            font.pointSize: 12
            color: "#444444"
            clip:true
            textFormat: Text.PlainText
            elide: Text.ElideMiddle
            font.family: openSansSemibold.name
        }

        Text {
            id: creationDate
            Layout.alignment: Qt.AlignRight
            horizontalAlignment: TextInput.AlignRight
            topPadding: 4
            anchors { top: creationDateLabel.top }
            Layout.fillWidth: true
            text:  Qt.formatDate(documentModel.creationDate, "d MMMM yyyy")
            color: "#8e8e8e"
            font.pointSize: 12
            clip: true
            textFormat: Text.PlainText
            elide: Text.ElideMiddle
            font.family: openSansSemibold.name
        }

        Label {
            id: statusLabel
            Layout.alignment: Qt.AlignLeft|Qt.AlignTop
            topPadding: 4
            text: "Document status:"
            font.pointSize: 12
            color: "#444444"
            clip:true
            textFormat: Text.PlainText
            elide: Text.ElideMiddle
            font.family: openSansSemibold.name
        }

        Text {
            id: status
            Layout.alignment: Qt.AlignRight
            horizontalAlignment: TextInput.AlignRight
            anchors { top: statusLabel.top}
            topPadding: 4
            clip: true
            textFormat: Text.PlainText
            elide: Text.ElideMiddle
            Layout.fillWidth: true
            text: { "Waiting for"/*documentModel.status*/ }
            color: "#8e8e8e"
            font.pointSize: 12
            font.family: openSansSemibold.name
        }
    }

    Label {
        id: messageLabel
        width: parent.width * 0.8
        anchors { top: mainInfoLayout.bottom; topMargin: 20; horizontalCenter: parent.horizontalCenter }
        Layout.alignment: Qt.AlignLeft
        Layout.minimumWidth: implicitWidth
        topPadding: 4
        text: "Message:"
        font.pointSize: 12
        color: "#444444"
        clip: true
        font.family: openSansSemibold.name
    }

    Rectangle {
        id: messageContainer
        anchors { top: messageLabel.bottom; topMargin: 7; horizontalCenter: parent.horizontalCenter }
        width: parent.width * 0.8
        height: message.height
        border.color: message.borderColor
        border.width: 1
        color: "white"
        clip: true

        TextViews.AutoScrollText {
            id: message
            width: parent.width
            maxNotScrolledLines: 3
            contentHeight: contentText.height
            contentWidth: parent.width - scrollBar.width
            contentText.wrapMode: Text.WordWrap
            contentText.textFormat: Text.PlainText
            contentText.color: "#444444"
            contentText.font.pointSize: 10
            contentText.text: documentModel.message
            contentText.font.family: openSansRegular.name
            Layout.fillWidth: true
            editable: documentModel.editable

            ScrollBar.vertical: ScrollBar {
                id:scrollBar
                hoverEnabled: true
                active: hovered
                width: 6
                contentItem: Rectangle {
                    color: scrollBar.pressed ? "#2678c0" : "#2980cc"
                }
                background: Rectangle {
                    color: "#ebebeb"
                }
                clip: false
                anchors.top: message.top
                anchors.right: message.right
                anchors.bottom: message.bottom
            }
        }
    }

    Flickable {
        property int scrollStep: signersListView.list.currentItem? signersListView.list.currentItem.height : 55

        id: flickable
        //        height: parent.height
        anchors { top: messageContainer.bottom; topMargin: 20; bottom: parent.bottom; left: parent.left; leftMargin: parent.width * 0.1; right: parent.right; rightMargin: 16 }
        //        Layout.fillHeight: true
        contentHeight: scrollLayout.height
        flickableChildren: scrollLayout
        flickableDirection: Flickable.VerticalFlick
        boundsBehavior: Flickable.StopAtBounds
        clip: true

        Component.onCompleted:{
            flickable.forceActiveFocus()
        }

        Keys.onUpPressed: {
            console.log("up")
            scrollUp();
        }

        Keys.onDownPressed: {
            console.log("down")
            scrollDown();
        }

        function scrollUp() {
            if (flickable.contentY > 0) {
                flickable.contentY -= scrollStep
            }
            if (flickable.contentY < 0) {
                flickable.contentY = 0
            }
        }

        function scrollDown() {
            var oopsMagicNumber = 65
            if (flickable.contentY < flickable.contentHeight - panel.height + oopsMagicNumber) {
                flickable.contentY += scrollStep
            }
            if (flickable.contentY > flickable.contentHeight - panel.height + oopsMagicNumber) {
                flickable.contentY = flickable.contentHeight - panel.height + oopsMagicNumber
            }
        }

        ScrollBar.vertical: ScrollBar {
            id: scrollBarFlickable
            Layout.fillHeight: true
            parent: flickable.parent
            hoverEnabled: true
            active: hovered
            clip: false
            width: 8
            size: 0.3
            contentItem: Rectangle {
                width: parent.width
                height: flickable.contentHeight
                color: scrollBarFlickable.pressed ? "#2678c0" : "#2980cc"
            }
            background: Rectangle {
                color: "#ebebeb"
            }
            anchors.top: flickable.top
            anchors.left: flickable.right
            anchors.bottom: flickable.bottom
            anchors.leftMargin: 9
        }

        ColumnLayout {
            id: scrollLayout
            spacing: 0
            width: parent.width

            ColumnLayout {
                id: authorLayout
                spacing: 0
                width: parent.width
                visible: !userIsAuthor

                Label {
                    id: authorLabel
                    Layout.alignment: Qt.AlignLeft
                    Layout.minimumWidth: implicitWidth
                    topPadding: 4
                    text: "Author:"
                    font.pointSize: 12
                    color: "#444444"
                    clip: true
                    font.family: openSansSemibold.name
                }

                Item {
                    height: 10
                    width: 1
                }

                ContactView {
                    id: userItem
                    model: authorModel
                    Layout.alignment: Qt.AlignLeft
                    anchors.topMargin: 20
                    colorIndication: false
                    statusIndication: false
                    anchors.left: parent.left
                    anchors.right: parent.rigth
                    Layout.preferredHeight: 35
                }

                Item {
                    height: 25
                    width: 1
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        flickable.forceActiveFocus();
                    }
                }

            }

            Label {
                id: signersLabel
                Layout.alignment: Qt.AlignLeft
                Layout.minimumWidth: implicitWidth
                topPadding: 4
                text: "Signers:"
                font.pointSize: 12
                color: "#444444"
                clip: true
                font.family: openSansSemibold.name
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        flickable.forceActiveFocus();
                    }
                }
            }

            Item {
                height: 10
                width: 1
            }

            ContactListView {
                id: signersListView
                height: list.count > 0? list.contentHeight : dummyList.height
                Layout.preferredHeight: height
                Layout.fillWidth: true
                colorIndication: true
                statusIndication: !documentModel.editable
                considerOrder: documentModel.hasOrder
                dividerVisible: false
                clickable: false
                list.interactive: false
                list.currentIndex: -1
                //                considerOrder: signOrderCheckbox.checked
                actionBtnVisibility: true
                actionBtnSource: "iconNotifications"
                onActionBtnClicked: {
                    console.log("notify participant with id " + contactId)
                }
                list.onCurrentIndexChanged: {
                    var currIndex = list.currentIndex
                    if (!contactsModel.hasIndex(currIndex, 0)) {
                        return
                    }
                    var currItem = contactsModel.itemAt(currIndex);
                    //                    chooseSigner(currItem.contactId)

                }

                contactsModel: signersListModel
            }

            Item {
                id: divider1
                height: 25
                width: 1
            }

            Label {
                id: observersLabel
                Layout.alignment: Qt.AlignLeft
                Layout.minimumWidth: implicitWidth
                topPadding: 4
                text: "Observers:"
                font.pointSize: 12
                color: "#444444"
                clip: true
                font.family: openSansSemibold.name
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        flickable.forceActiveFocus();
                    }
                }
            }

            Item {
                id: divider2
                height: 10
                width: 1
            }

            ContactListView {
                id: observersListView
                height: list.contentHeight
                Layout.preferredHeight: height
                Layout.fillWidth: true
                colorIndication: false
                dividerVisible: false
                clickable: false
                list.interactive: false
                list.currentIndex: -1
                onActionBtnClicked: {
                    console.log("notify participant with id " + contactId)
                }

                list.onCurrentIndexChanged: {
                    var currIndex = list.currentIndex
                    if (!contactsModel.hasIndex(currIndex, 0)) {
                        return
                    }
                    var currItem = contactsModel.itemAt(currIndex);
                    //                    chooseSigner(currItem.contactId)

                }
                contactsModel: observersListModel
            }

            Item {
                id: empty_item
                height: 125
                width: 1
            }
        }
    }

}
