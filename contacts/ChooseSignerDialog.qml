import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import com.contacts 1.0
import "./../texts" as TextViews
import "./../buttons" as Buttons
import "." as Contacts

Item {
    id: root
    objectName: "rootLayout"
    enabled: !busyIndicator.running
    width: 256
    height: 340

    signal chooseSigner(int contactId)
    signal addSignerFromContacts(int contactId)
    signal addNewContact(string email)

    function startBusyIndication() {
        busyIndicator.running = true
    }

    function finishBusyIndication() {
        busyIndicator.running = false
    }

    FontLoader {
        id: openSansRegular
        source: "OpenSansFonts/OpenSans-Regular.ttf"
    }

    MouseArea {
        id: bodyArea
        anchors.fill: parent
        onClicked: {
            console.log("forceActiveFocus");
            root.forceActiveFocus();
        }
    }

    ToolBar {
        id: toolBar
        width: parent.width * 0.85
        height: 32
        anchors { top: parent.top; topMargin: 16; horizontalCenter: parent.horizontalCenter }
        background: Rectangle{
            anchors.fill: parent
            color: "white"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("forceActiveFocus toolBar");
                    toolBar.forceActiveFocus();
                }
            }
        }
        Label {
            width: parent.width
            color: "#444444"
            text: "<b>Choose signer</b>"
            anchors.verticalCenter: parent.verticalCenter
            font.pointSize: 14
            font.family: openSansRegular.name
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            id: closeBtn
            objectName: "closeBtn"
            signal closed()
            width: 10
            height: 10
            anchors { right: parent.right; verticalCenter: parent.verticalCenter }
            Image {
                id: closeImg
                width: parent.width
                height: parent.height
                sourceSize.width: width
                sourceSize.height: height
                antialiasing: true
                fillMode: Image.PreserveAspectFit
                source: "iconWindowCloseDark"
            }
            // Respond to the signal here.
            onClicked: {
                closed()
                chooseSigner(0) //for anonymous user
            }
        }
    }

    Item {
        width: parent.width * 0.8 + 20
        //        height: parent.height
        visible: listView.list.count > 0
        anchors { left: parent.left; leftMargin: parent.width * (0.2 / 2); top: inputLayout.bottom; topMargin: 24 ; bottom: parent.bottom }

        Contacts.ContactListView {
            id: listView
            scrollBarWidth: 6
            function chooseCurrentSigner() {
                var currIndex = list.currentIndex
                if (!contactsModel.hasIndex(currIndex, 0)) {
                    return
                }
                var currItem = contactsModel.itemAt(currIndex);
                chooseSigner(currItem.contactId)
            }

            onItemClicked: { chooseCurrentSigner() }
//            enabled: !busyIndicator.running
            list.currentIndex: -1
            colorIndication: true
            dividerVisible: false
            Keys.onReturnPressed: chooseCurrentSigner()
            anchors { bottom: parent.bottom; bottomMargin: 10 }
            list.onCurrentIndexChanged: {

            }
            contactsModel: contactListModel
        }
    }

    TextViews.InputFieldWithSuggestions {
        id: inputLayout
        objectName: "inputLayout"
        width: parent.width * 0.8
        inputField.hintText: "Invite via email"
        anchors { horizontalCenter: parent.horizontalCenter; top: toolBar.bottom; topMargin: 24 }

        suggestionsModel: suggestionListModel
        onSuggestionChosen: {
            addSignerFromContacts(1 + (Math.random() * 9999))
        }
        onUserInputChosen: {
            var email = inputField.text.trim()
            if (email == "") {
                inputField.warningText = "Invalid email!"
                return
            }
            addNewContact(email)
        }
    }

    BusyIndicator {
        id: busyIndicator
        objectName: "busyIndicator"
        height: parent.height
        width: parent.width
        focusPolicy: Qt.WheelFocus
        background: Rectangle {
            color: "black"
            opacity: 0.2
        }
        padding: width * 0.4
        focus: running
        visible: running
        running: false
    }
}
