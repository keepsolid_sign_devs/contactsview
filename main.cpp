#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QWindow>
#include <QDesktopWidget>
#include <QWidget>
#include <QStyle>
#include <QSortFilterProxyModel>
#include <QQuickItem>
#include <QQuickWindow>

#include "contactlistmodel.h"
#include "contactsortfiltermodel.h"
#include "contactscontroller.h"
#include "maincontroller.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);

    MainController mainController;
    mainController.onShowDocumentTitleBar(10);
    mainController.onShowContactsScreen();
    mainController.onShowDocumentInfo(10);
//    mainController.onShowManageParticipantsDialog(10);
    mainController.onShowPageStatusBar();
    mainController.pagesCountChanged(10);
    mainController.currentPageNumberChanged(6);

    return app.exec();
}

