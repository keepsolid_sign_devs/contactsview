#include <QHash>

#include "suggestionlistmodel.h"

SuggestionListModel::SuggestionListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

SuggestionListModel::~SuggestionListModel()
{
    for(int i = 0; i < dataList.count(); i++) {
        delete dataList.at(i);
    }
}

QHash<int, QByteArray> SuggestionListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[IDRole] = "id";
    return roles;
}

int SuggestionListModel::rowCount(const QModelIndex &parent) const
{
    return dataList.count();
}

QVariant SuggestionListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() < 0 || index.row() > dataList.count())
        return QVariant();

    const SuggestionModel *suggestion = itemAt(index.row());
    if (role == NameRole)
        return suggestion->name();
    else if (role == IDRole)
        return  QVariant::fromValue(suggestion->suggestionId());

    return QVariant();
}

Qt::ItemFlags SuggestionListModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    if (index.row() != index.column())
        flags |= Qt::ItemIsEditable;
    return flags;
}




