import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import com.contacts 1.0
import "./contacts" as Contacts
import "./texts" as TextViews
import "./buttons" as Buttons
import "./styles"

Item {

    height: parent? parent.height : 0
    width: parent? parent.width : 0

    FontLoader {
        id: openSansRegular
        source: "OpenSansFonts/OpenSans-Regular.ttf"
    }

    Rectangle {
        anchors.fill: parent
        color: "white"
    }

    Label {
        text: qsTr("Empty list. You can add contacts manually")
        padding: 40
        visible: listView.list.count < 1
        width: parent.width
        height: parent.height

        horizontalAlignment: Text.AlignHCenter
        elide: Text.ElideRight
        wrapMode: Text.WordWrap
        textFormat: Text.PlainText
        clip: true
        color: "#444444"
        font.pointSize: 14
        font.family: openSansRegular.name

        background: Rectangle {
            color: "#ebebeb"
        }

        Image {
            width: 300
            height: 300
            fillMode: Image.PreserveAspectFit
            anchors.centerIn: parent
            source: "contacts/empty_contact_list_icon"
        }
    }


    RowLayout {
        spacing: 0
        anchors { topMargin: 6; top: parent.top; bottom: parent.bottom }
        width: parent.width
        height: parent.height

        Item {
            height: 1
            width: 10
        }

        Item {
            Layout.fillHeight: true
            Layout.preferredWidth: parent.width * 0.4
            Layout.minimumWidth: 250
            visible: listView.list.count > 0

            Contacts.ContactListView {
                id: listView
                scrollBarWidth: 10
                list.onCurrentIndexChanged: {
                    //                    var currIndex = contactsModel.mapToSource(contactsModel.index(list.currentIndex, 0)).row;
                    var currIndex = list.currentIndex
                    console.log(currIndex);
                    if (!contactsModel.hasIndex(currIndex, 0)) {
                        return
                    }
                    contactInfoItem.readOnly = true
                    //                    var currItem = contactsModel.sourceModel.itemAt(currIndex);
                    var currItem = contactsModel.itemAt(currIndex);
                    // @disable-check M126
                    currContactIcon.source = currItem.iconSource? "contacts/" + currItem.iconSource : "contacts/default_contact_icon"
                    currContactIcon.hasIcon = currItem.iconSource
                    currContactName.text = currItem.firstName + " " + currItem.lastName
                    currContactEmail.text = currItem.email
                    currContactCompany.text = currItem.company? currItem.company : "No company"
                    rightBtn.visible = true
                    leftBtn.visible = true
                }
                contactsModel: contactListModel
            }
        }

        Item {
            id: contactInfoItem
            property bool readOnly: true
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.bottomMargin: 20
            Layout.leftMargin: 20
            Layout.rightMargin: 10
            Layout.preferredWidth: contactColumn.implicitWidth
            clip: true
            visible: listView.list.count > 0

            Rectangle {
                anchors { fill: parent }
                color: "#f8f8f8"
            }

            ColumnLayout {
                id: contactColumn
                anchors { fill: parent }
                spacing: 0

                Item {
                    width: 1
                    height: 40
                }

                Rectangle {
                    id: currContactIconBackground
                    width: parent.width * 0.1 < 72? 72 : parent.width * 0.1
                    height:  width
                    Layout.alignment: Qt.AlignHCenter
                    radius: width / 2
                    antialiasing: true
                    color: "white"

                    Image {
                        id: currContactIcon
                        property bool hasIcon: false
                        width: hasIcon? parent.width - 6 : parent.width
                        height: hasIcon? parent.height - 6 : parent.height
                        fillMode: Image.PreserveAspectFit
                        antialiasing: true
                        anchors { centerIn: parent }
                        sourceSize.width: width
                        sourceSize.height: height
                    }
                }

                Item {
                    width: 1
                    height: 20
                }

                TextViews.TextWithTooltip {
                    id: currContactName
                    Layout.fillWidth: true
                    horizontalAlignment: TextInput.AlignHCenter
                    visible: contactInfoItem.readOnly
                    text: "Stewe Webb"
                    color: "#444444"
                    font.pointSize: 24
                }

                Item {
                    width: 1
                    height: 34
                }

                GridLayout {
                    id: fieldsLayout
                    columns: 2
                    rows:2
                    rowSpacing: 15
                    columnSpacing: 20
                    Layout.leftMargin: 24
                    Layout.maximumWidth: parent.width * 0.8
                    Layout.alignment: Qt.AlignHCenter

                    Label {
                        id: nameLabel
                        Layout.alignment: Qt.AlignLeft|Qt.AlignTop
                        topPadding: 4
                        Layout.minimumWidth: implicitWidth
                        visible: !contactInfoItem.readOnly
                        text: "Name:"
                        font.pointSize: 14
                        color: "#5e5e5e"
                        clip:true
                        font.family: openSansRegular.name
                    }

                    TextViews.InputField {
                        id: currNameField
                        Layout.fillWidth: true
                        visible: !contactInfoItem.readOnly
                        width: parent.width
                        hintText: "Enter name"
                    }

                    Label {
                        id: surnameLabel
                        Layout.alignment: Qt.AlignLeft|Qt.AlignTop
                        Layout.minimumWidth: implicitWidth
                        topPadding: 4
                        visible: !contactInfoItem.readOnly
                        text: "Surname:"
                        font.pointSize: 14
                        color: "#5e5e5e"
                        clip:true
                        font.family: openSansRegular.name
                    }

                    TextViews.InputField {
                        id: currSurnameField
                        Layout.fillWidth: true
                        visible: !contactInfoItem.readOnly
                        width: parent.width
                        hintText: "Enter surname"
                    }

                    Label {
                        id: companyLabel
                        Layout.alignment: Qt.AlignLeft|Qt.AlignTop
                        Layout.minimumWidth: implicitWidth
                        topPadding: 4
                        text: "Company:"
                        font.pointSize: 14
                        color: "#5e5e5e"
                        clip:true
                        font.family: openSansRegular.name
                    }

                    TextViews.TextWithTooltip {
                        id: currContactCompany
                        Layout.alignment: Qt.AlignLeft
                        visible: contactInfoItem.readOnly
                        topPadding: 4
                        anchors { top: companyLabel.top }
                        Layout.fillWidth: true
                        text: "The Short Films of David Lynch & The Lime Green Set"
                        color: "#2a2a2a"
                        font.pointSize: 14
                    }

                    TextViews.InputField {
                        id: currCompanyField
                        Layout.fillWidth: true
                        visible: !contactInfoItem.readOnly
                        width: parent.width
                        hintText: "Enter company"
                    }

                    Label {
                        id: emailLabel
                        Layout.alignment: Qt.AlignLeft|Qt.AlignTop
                        Layout.minimumWidth: implicitWidth
                        topPadding: 4
                        text: "Email:"
                        font.pointSize: 14
                        color: "#5e5e5e"
                        clip:true
                        font.family: openSansRegular.name
                    }

                    TextViews.TextWithTooltip {
                        id: currContactEmail
                        Layout.alignment: Qt.AlignLeft
                        visible: contactInfoItem.readOnly
                        anchors { top: emailLabel.top}
                        topPadding: 4
                        Layout.fillWidth: true
                        text: "The Short Films of David Lynch & The Lime Green Set"
                        color: "#2a2a2a"
                        font.pointSize: 14
                    }

                    TextViews.InputField {
                        id: currEmailField
                        Layout.fillWidth: true
                        visible: !contactInfoItem.readOnly
                        //                        textEdit.anchors { verticalCenter: emailLabel.verticalCenter }
                        width: parent.width
                        hintText: "Enter email"
                    }
                }

                Item {
                    Layout.fillHeight: true
                }

                RowLayout {
                    spacing: 0
                    Layout.fillWidth: true
                    layoutDirection: Qt.RightToLeft

                    Item {
                        height: 1
                        width: 10
                    }

                    Buttons.StateButton {
                        id: rightBtn
                        objectName: "rightBtn"
                        signal deleteContact(int index)
                        signal editContact(int index)

                        stateStyle: contactInfoItem.readOnly? Styles.warning : Styles.primary
                        contentText: contactInfoItem.readOnly? "Delete" : "Save"

                        onClicked: {
                            if (contactInfoItem.readOnly) {
                                //                                deleteContact(listView.contactsModel.mapToSource(listView.contactsModel.index(listView.list.currentIndex, 0)).row)
                                deleteContact(listView.list.currentIndex)
                                listView.list.currentIndexChanged()
                            } else {
                                if (currEmailField.text=="") {
                                    currEmailField.warningText = "Something is wrong"
                                    return
                                }
                                //                                var currIndex = listView.contactsModel.index(listView.list.currentIndex, 0)
                                var currIndex = listView.list.currentIndex
                                //                                var currRealIndex = listView.contactsModel.mapToSource(listView.contactsModel.index(listView.list.currentIndex, 0)).row
                                //                                var currItem = listView.contactsModel.sourceModel.itemAt(currRealIndex)
                                //                                var currItem = listView.contactsModel.sourceModel.itemAt(currIndex)
                                var currItem = listView.contactsModel.itemAt(currIndex)
                                currItem.email = currEmailField.text
                                currItem.firstName = currNameField.text
                                currItem.lastName = currSurnameField.text
                                currItem.company = currCompanyField.text
                                currContactName.text = currItem.firstName + " " + currItem.lastName
                                currContactEmail.text = currItem.email
                                currContactCompany.text = currItem.company? currItem.company : "No company"
                                listView.contactsModel.dataChanged(listView.contactsModel.index(currIndex, 0), listView.contactsModel.index(currIndex, 0))
                                contactInfoItem.readOnly = true
                            }

                        }
                    }

                    Component {
                        id: contactComponent
                        ContactModel {}
                    }

                    Item {
                        height: 1
                        width: 10
                    }

                    Buttons.StateButton {
                        id: leftBtn
                        objectName: "leftBtn"
                        stateStyle: Styles.secondary
                        contentText: contactInfoItem.readOnly? "Edit" : "Cancel"
                        onClicked:  {
                            contactInfoItem.readOnly = !contactInfoItem.readOnly
                            if (!contactInfoItem.readOnly) {
                                //                                var currIndex = listView.contactsModel.mapToSource(listView.contactsModel.index(listView.list.currentIndex, 0)).row
                                var currIndex = listView.list.currentIndex
                                //                                var currItem = listView.contactsModel.sourceModel.itemAt(currIndex)
                                var currItem = listView.contactsModel.itemAt(currIndex)
                                currEmailField.text = currItem.email
                                currNameField.text = currItem.firstName
                                currSurnameField.text = currItem.lastName
                                currCompanyField.text = currItem.company
                            }
                        }
                    }

                    Item {
                        Layout.fillWidth: true
                    }
                }

                Item {
                    height: 10
                    width: 1
                }
            }

        }
    }
}
