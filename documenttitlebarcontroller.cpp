#include "documenttitlebarcontroller.h"

DocumentTitleBarController::DocumentTitleBarController(QObject* _mainView, QObject* _parent)
    : QObject(_parent)
{
    mainView = _mainView;
    registerTypes();
    fillData();
    setupUI();
    setupConnections();
}

DocumentTitleBarController::DocumentTitleBarController(int documentId, QObject* _mainView, QObject* _parent)
    : DocumentTitleBarController(_mainView, _parent)
{
}

DocumentTitleBarController::~DocumentTitleBarController()
{
    qDebug("DocumentTitleBarController destructor");
    delete document;
}

void DocumentTitleBarController::registerTypes()
{
    qmlRegisterType<DocumentModel>("com.contacts", 1, 0, "DocumentModel");
}

void DocumentTitleBarController::fillData()
{
    document = new DocumentModel();
    document->setName("Doooooc");
    document->setCreationDate(QDate::currentDate());
    document->setHasOrder(true);
    document->setMessage("Message\nololol\nmmoomom\nsdfksdfklsdf\nhateit");
    document->setStatus(2);
    document->setEditable(false);
}

void DocumentTitleBarController::setupUI()
{
    mainView->setProperty("document",  QVariant::fromValue<QObject*>(document));
}

void DocumentTitleBarController::setupConnections()
{
    connect(mainView, SIGNAL(undoClicked()), this, SLOT(onUndoClicked()));
    connect(mainView, SIGNAL(redoClicked()), this, SLOT(onRedoClicked()));
    connect(mainView, SIGNAL(printClicked()), this, SLOT(onPrintClicked()));
    connect(mainView, SIGNAL(useClicked()), this, SLOT(onUseClicked()));
}

void DocumentTitleBarController::onUndoClicked()
{
    qDebug("onUndoClicked");
}

void DocumentTitleBarController::onRedoClicked()
{
    qDebug("onRedoClicked");
}

void DocumentTitleBarController::onPrintClicked()
{
     qDebug("onPrintClicked");
}

void DocumentTitleBarController::onUseClicked()
{
    qDebug("onUseClicked");
}
