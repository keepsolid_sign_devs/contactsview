pragma Singleton
import QtQuick 2.7

QtObject {
    id: buttonTypes
    readonly property ButtonStateStyle primary: ButtonStateStyle {
        disabledColorBkg: "#2980cc"
        clickedColorBkg: "#2370b3"
        hoveredColorBkg: "#2e90e6"
        normalColorBkg: "#2980cc"
        textColor: "white"
    }
    readonly property ButtonStateStyle secondary: ButtonStateStyle {
        disabledColorBkg: "#ebebeb"
        clickedColorBkg: "#dedede"
        hoveredColorBkg: "#f5f5f5"
        normalColorBkg: "#ebebeb"
        textColor: "#444444"
    }
    readonly property ButtonStateStyle cta: ButtonStateStyle {
        disabledColorBkg: "#22d63a"
        clickedColorBkg: "#1dbf47"
        hoveredColorBkg: "#20d951"
        normalColorBkg: "#22d63a"
        textColor: "white"
    }
    readonly property ButtonStateStyle warning: ButtonStateStyle {
        disabledColorBkg: "#ebebeb"
        clickedColorBkg: "#dedede"
        hoveredColorBkg: "#f5f5f5"
        normalColorBkg: "#ebebeb"
        textColor: "#f02b2b"
    }
}
