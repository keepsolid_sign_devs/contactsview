import QtQuick 2.7

QtObject {
    property color disabledColorBkg
    property color clickedColorBkg
    property color hoveredColorBkg
    property color normalColorBkg
    property color textColor
}
