#ifndef PAGESTATUSBARCONTROLLER_H
#define PAGESTATUSBARCONTROLLER_H

#include <QObject>
#include <QQuickView>
#include <QQmlApplicationEngine>

class PageStatusBarController: public QObject
{
    Q_OBJECT

public:
    explicit PageStatusBarController(QObject* _parent = 0);
    ~PageStatusBarController();

private:
    void registerTypes();
    void fillData();
    void setupUI();
    void setupConnections();

signals:
    void currentPageNumberChanged(QVariant number);
    void pagesCountChanged(QVariant count);

public slots:
    void onZoomValueChanged(int value);
    void onCurrentPageNumberChanged(int number);
    void onPagesCountChanged(int count);

private:
    QObject* mainView;
    QQmlApplicationEngine engine;

};

#endif // PAGESTATUSBARCONTROLLER_H
