#ifndef ADDCONTACTCONTROLLER_H
#define ADDCONTACTCONTROLLER_H

#include <QObject>
#include <QQmlContext>
#include <QWidget>
#include <QQuickWidget>
#include <QQuickView>
#include <QQmlApplicationEngine>

#include "contactsortfiltermodel.h"
#include "contactlistmodel.h"

class AddContactController: public QObject
{
    Q_OBJECT

public:
    explicit AddContactController(QObject* _parent = 0);
    ~AddContactController();

private:
    void registerTypes();
    void setupUI();
    void setupConnections();
    bool validateFields();

signals:
    void addedContact(ContactModel*);
    void openContactBook();
    void startBusyIndication();
    void finishBusyIndication();

public slots:
    void onAddContact(ContactModel*);
    void onOpenContactBook();
    void onClosed();

private:
    QQuickView* mainView;
    QQmlApplicationEngine engine;

};

#endif // ADDCONTACTCONTROLLER_H
