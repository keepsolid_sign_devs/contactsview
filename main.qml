import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import com.contacts 1.0
import "./contacts" as Contacts
import "."

ApplicationWindow {
    id: mainWindow
    visible: true
    width: 1000
    height: 700
    flags: Qt.FramelessWindowHint
    title: qsTr("Contacts")


    header: ToolBar {
        background: Rectangle {
            color: "white"
        }
        FastBlur {
            id: titleBarBlur
            anchors.fill: titleBar
            radius: 3
            source: ShaderEffectSource {
                sourceItem: mainLayout
                sourceRect: Qt.rect(0, 0, titleBarBlur.width, titleBarBlur.height)
            }
        }
        DocumentToolbar {
            id: titleBar
            objectName: "documentTitleBar"
            attachedWindow: mainWindow
            width: mainWindow.width
            height: 27
        }
        height: titleBar.height + searchView.height
        objectName: "toolbar"
        TextField {
            signal searchTextChanged(string query)
            id: searchView
            objectName: "searchView"
            placeholderText: "Search contacts..."
            selectByMouse: true
            activeFocusOnPress: true
            anchors.top: titleBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 10
            color: "#444444"
            font.pointSize: 12
            onTextChanged: searchTextChanged(text.trim())
        }
    }

    Item {
        id: mainLayout
        anchors { fill: parent; bottom: footer.top; top: header.bottom; topMargin: 1  }

        Item {
            id: container
            objectName: "container"
            anchors { top: parent.top; bottom: parent.bottom; right: panel.left; left: parent.left }
        }

        Rectangle {
            id: divider
            width: 1
            color: "#dbdbdb"
            anchors { right: panel.left; bottom: parent.bottom; top: parent.top }
        }

        Item {
            id: panel
            objectName: "panel"

            width: parent.width * 0.3 > Layout.maximumWidth? Layout.maximumWidth : parent.width * 0.3
            anchors { right: parent.right; top: parent.top;  bottom: parent.bottom }
        }

    }

    FastBlur {
        id: statusBarBlur
        anchors.fill: statusBar
        radius: 3
        source: ShaderEffectSource {
            sourceItem: mainLayout
            sourceRect: Qt.rect(0, mainLayout.height - statusBarBlur.height, statusBarBlur.width, statusBarBlur.height)
        }
    }

    Item {
        id: statusBar
        objectName: "statusBar"
        width: parent.width
        height: 25
        anchors { bottom: parent.bottom }
    }

    footer: KSMainFooter {
        id: footer
        objectName: "Footer"
        addButtonVisible: true
        addButtonText: "Add contact"
    }

}
