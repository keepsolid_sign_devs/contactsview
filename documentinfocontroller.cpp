#include "documentinfocontroller.h"
#include "documentmodel.h"

DocumentInfoController::DocumentInfoController(QObject* _parent)
    : QObject(_parent)
{
    registerTypes();
    fillData();
    setupUI();
    setupConnections();
}

DocumentInfoController::DocumentInfoController(int documentId, QObject* _parent)
    : DocumentInfoController(_parent)
{
}

DocumentInfoController::~DocumentInfoController()
{
    qDebug("ManageParticipantsController destructor");
}

void DocumentInfoController::registerTypes()
{
    qmlRegisterType<ContactModel>("com.contacts", 1, 0, "ContactModel");
    qmlRegisterType<ContactListModel>("com.contacts", 1, 0, "ContactListModel");
    qmlRegisterType<SuggestionListModel>("com.contacts", 1, 0, "SuggestionListModel");
    qmlRegisterType<SuggestionModel>("com.contacts", 1, 0, "SuggestionModel");
    qmlRegisterType<DocumentModel>("com.contacts", 1, 0, "DocumentModel");
}

void DocumentInfoController::fillData()
{
    long id = 1;
    signersModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255), 4);
    signersModel.addContact(id++, "JohnJohnJohn", "Doe", "example@gmail.com", "KeepSolidDoe", "sad_person", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255), 4);
    signersModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255), 3);
    signersModel.addContact(id++, "JohnJohnJohn", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255), 2);
    signersModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255), 2);

    observersModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "sad_person", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    observersModel.addContact(id++, "JohnJohnJohn", "Doe", "example@gmail.com", "KeepSolidDoe", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    observersModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "sad_person", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
}

void DocumentInfoController::setupUI()
{
    engine.rootContext()->setContextProperty("signersListModel", &signersModel);
    engine.rootContext()->setContextProperty("observersListModel", &observersModel);

    ContactModel* author = new ContactModel(rand() % 2550, "Author", "Real", "exammple@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    engine.rootContext()->setContextProperty("authorModel", author);

    DocumentModel* document = new DocumentModel();
    document->setName("Doooooc");
    document->setCreationDate(QDate::currentDate());
    document->setHasOrder(true);
    document->setMessage("Message\nololol\nmmoomom\nsdfksdfklsdf\nhateit");
    document->setStatus(2);
    document->setEditable(false);
    engine.rootContext()->setContextProperty("documentModel", document);

    QQmlComponent newcomp(&engine, QUrl(QLatin1String("qrc:/contacts/DocumentInfoPanel.qml")), parent());
    mainView = newcomp.create();
    mainView->setProperty("parent", QVariant::fromValue<QObject*>(parent()));

    QQmlEngine::setObjectOwnership(mainView, QQmlEngine::CppOwnership);
}

void DocumentInfoController::setupConnections()
{
    QObject* rootLayout = mainView;

    QObject* closeBtn = rootLayout->findChild<QObject*>("closeBtn");
    connect(closeBtn, SIGNAL(closed()), this, SLOT(onClosed()));

//    connect(rootLayout, SIGNAL(signOrderChanged(bool)), this, SLOT(onSignOrderChanged(bool)));
}

void DocumentInfoController::onClosed()
{
    qDebug("onClosed");
}
