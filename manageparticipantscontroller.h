#ifndef MANAGEPARTICIPANTSCONTROLLER_H
#define MANAGEPARTICIPANTSCONTROLLER_H

#include <QObject>
#include <QQuickView>
#include <QQmlApplicationEngine>

#include "contactmodel.h"
#include "contactlistmodel.h"
#include "suggestionlistmodel.h"

class ManageParticipantsController: public QObject
{
    Q_OBJECT

public:
    explicit ManageParticipantsController(QObject* _parent = 0);
    ManageParticipantsController(int documentId, QObject* _parent = 0);
    ~ManageParticipantsController();

private:
    void registerTypes();
    void fillData();
    void setupUI();
    void setupConnections();

signals:
    void signOrderChanged(bool isImportant);

    void participantRoleChanged(int contactId, int role);
    void participantChanged(int contactId);
    void nextStepNecessityChanged(bool isNeeded);
    void openedParticipantDialog(int role);

    void startBusyIndicationFor(QVariant role);
    void finishBusyIndication();

public slots:
    void onSignOrderChanged(bool isImportant);

    void onParticipantRoleChanged(int contactId, int role);
    void onParticipantPositionChanged(int contactId, int position);

    void onAddParticipant(int contactId, int role);
    void onAddNewContact(QString email, int role);
    void onOpenAddParticipantDialog(int role);
    void onRemoveParticipant(int contactId, int role);

    void setNextStepNecessity(bool isNeeded);
    void onClosed();

private:
    ContactListModel observersModel;
    ContactListModel signersModel;
    SuggestionListModel suggestionListModel;
    QObject* mainView;
    QQmlApplicationEngine engine;
    const int SIGNER = 0;
    const int OBSERVER = 1;
    bool nextStepNecessity;

};

#endif // MANAGEPARTICIPANTSCONTROLLER_H
