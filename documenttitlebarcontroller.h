#ifndef DOCUMENTTITLEBARCONTROLLER_H
#define DOCUMENTTITLEBARCONTROLLER_H

#include <QObject>
#include <QQuickView>
#include <QQmlContext>
#include <QQmlComponent>
#include <QQmlApplicationEngine>

#include "documentmodel.h"

class DocumentTitleBarController: public QObject
{
    Q_OBJECT

public:
    explicit DocumentTitleBarController(QObject* mainView, QObject* _parent = 0);
    DocumentTitleBarController(int documentId, QObject* mainView, QObject* _parent = 0);
    ~DocumentTitleBarController();

private:
    void registerTypes();
    void fillData();
    void setupUI();
    void setupConnections();

signals:
//    void signOrderChanged(bool isImportant);


public slots:
    void onUndoClicked();
    void onRedoClicked();
    void onPrintClicked();
    void onUseClicked();

private:
    QObject* mainView;
    QQmlApplicationEngine engine;
    DocumentModel* document;
};

#endif // DOCUMENTTITLEBARCONTROLLER_H
