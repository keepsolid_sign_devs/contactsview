#include <QHash>

#include "contactlistmodel.h"

ContactListModel::ContactListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

ContactListModel::~ContactListModel()
{
    for(int i = 0; i < dataList.count(); i++) {
        delete dataList.at(i);
    }
}

QHash<int, QByteArray> ContactListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[FirstNameRole] = "firstName";
    roles[LastNameRole] = "lastName";
    roles[EmailRole] = "email";
    roles[CompanyRole] = "company";
    roles[ColorRole] = "color";
    roles[StatusRole] = "status";
    roles[IconRole] = "iconSource";
    return roles;
}

int ContactListModel::rowCount(const QModelIndex &parent) const
{
    return dataList.count();
}

QVariant ContactListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() < 0 || index.row() > dataList.count())
        return QVariant();

    const ContactModel *contact = itemAt(index.row());
    if (role == FirstNameRole)
        return contact->firstName();
    else if (role == LastNameRole)
        return contact->lastName();
    else if (role == EmailRole)
        return contact->email();
    else if (role == CompanyRole)
        return contact->company();
    else if (role == IconRole)
        return contact->iconSource();
    else if (role == StatusRole)
        return  QVariant::fromValue(contact->status());
    else if (role == ColorRole)
        return  QVariant::fromValue(contact->color());

    return QVariant();
}

Qt::ItemFlags ContactListModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    if (index.row() != index.column())
        flags |= Qt::ItemIsEditable;
    return flags;
}

void ContactListModel::move(int oldIndex, int newIndex) {
    if (oldIndex < 0 || oldIndex >= rowCount() || newIndex < 0 || newIndex >= rowCount()) return;
    QModelIndex parent;
//    beginMoveRows(parent, oldIndex, oldIndex, parent, newIndex);
    beginResetModel();
    dataList.move(oldIndex, newIndex);
    endResetModel();
//    endMoveRows();
}

void ContactListModel::addContact(ContactModel* contact)
{
    addContact(contact->contactId(), contact->firstName(), contact->lastName(), contact->email(), contact->company(), contact->iconSource(),
               contact->color(), contact->status());
}

void ContactListModel::addContact(const int& contactId, const QString &firstName, const QString &lastName, const QString &email, const QString &company,  const QString &iconSource,
                                  const QColor &color, const int &status)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    dataList.append(new ContactModel(contactId, firstName, lastName, email, company, iconSource, color, status));
    endInsertRows();
}

void ContactListModel::addContact(const int& contactId, const QString &firstName, const QString &lastName, const QString &email, const QString &company, const QString &iconSource, const QColor &color)
{
    addContact(contactId, firstName, lastName, email, company, iconSource, color, 0);
}

bool ContactListModel::removeContactAt(const int& index) {
    if (index < 0 || index >= rowCount()) return false;
    beginRemoveRows(QModelIndex(), index, index);
    ContactModel* contact = dataList.at(index);
    dataList.removeAt(index);
    endRemoveRows();

    delete contact;
    return true;
}

bool ContactListModel::editContactAt(const int& index) {
    if (index < 0 || index >= rowCount()) return false;
//    dataChanged(index, index);
    return true;
}


