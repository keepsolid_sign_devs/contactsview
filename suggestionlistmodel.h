#ifndef SUGGESTIONLISTMODEL_H
#define SUGGESTIONLISTMODEL_H

#include <QAbstractListModel>
#include <QList>
#include <memory>

#include "suggestionmodel.h"

class SuggestionListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum ContactRoles {
        NameRole = Qt::UserRole,
        IDRole = Qt::UserRole + 1
    };

    SuggestionListModel(QObject *parent = 0);
    ~SuggestionListModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    Q_INVOKABLE SuggestionModel* itemAt(int index) const {
        return dataList.at(index);
    }

    Q_INVOKABLE void clear() {
        qDebug("clear");
        beginRemoveRows(QModelIndex(), 0, rowCount());
        dataList.clear();
        endRemoveRows();
    }

    Q_INVOKABLE void addSuggestion(const int& id, const QString &name)
    {
        beginInsertRows(QModelIndex(), rowCount(), rowCount());
        dataList.append(new SuggestionModel(id, name));
        endInsertRows();
    }

    Q_INVOKABLE SuggestionModel* itemById(int id) const {
        for (SuggestionModel* item: dataList) {
            if (item->suggestionId() == id) {
                return item;
            }
        }
        qDebug("Suggestion with id %d not found", id);
        return NULL;
    }

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    QList<SuggestionModel*> dataList;
};


#endif // SUGGESTIONLISTMODEL_H
