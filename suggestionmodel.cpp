#include "suggestionmodel.h"

SuggestionModel::SuggestionModel(QObject *parent)
    : QObject(parent)
{
}

SuggestionModel::SuggestionModel(const int& suggestionId, const QString &name, QObject *parent)
    : QObject(parent), m_suggestion_id(suggestionId), m_name(name)
{
}

SuggestionModel::~SuggestionModel()
{
}

int SuggestionModel::suggestionId() const
{
    return m_suggestion_id;
}

QString SuggestionModel::name() const
{
    return m_name;
}

void SuggestionModel::setSuggestionId(const int &suggestionId)
{
    if (suggestionId != m_suggestion_id) {
        m_suggestion_id = suggestionId;
        emit suggestionIdChanged();
    }
}

void SuggestionModel::setName(const QString &name)
{
    if (name != m_name) {
        m_name = name;
        emit nameChanged();
    }
}
