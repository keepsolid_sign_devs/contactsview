#ifndef CONTACTSSORTFILTERMODEL_H
#define CONTACTSSORTFILTERMODEL_H

#include <QObject>
#include <QSortFilterProxyModel>

#include "contactsortfiltermodel.h"

class ContactSortFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT
};

#endif // CONTACTSSORTFILTERMODEL_H
