#include <QApplication>
#include <QStyle>
#include <QQuickItem>
#include <QQuickView>
#include <QTimer>
#include <QDesktopWidget>
#include <QGraphicsBlurEffect>

#include "pagestatusbarcontroller.h"

PageStatusBarController::PageStatusBarController(QObject* _parent) : QObject(_parent)
{
    registerTypes();
    setupUI();
    setupConnections();
}

PageStatusBarController::~PageStatusBarController()
{
    delete mainView;
}

void PageStatusBarController::registerTypes()
{
    qmlRegisterType<QGraphicsBlurEffect>("Effects", 1, 0, "Blur");
}

void PageStatusBarController::setupUI()
{
    QQmlComponent newcomp(&engine, QUrl(QLatin1String("qrc:/PageStatusBar.qml")), parent());
    mainView = newcomp.create();
    mainView->setProperty("parent", QVariant::fromValue<QObject*>(parent()));

    QQmlEngine::setObjectOwnership(mainView, QQmlEngine::CppOwnership);
}

void PageStatusBarController::setupConnections()
{
    connect(this, SIGNAL(currentPageNumberChanged(QVariant)), mainView, SLOT(changeCurrentPageNumber(QVariant)));
    connect(this, SIGNAL(pagesCountChanged(QVariant)), mainView, SLOT(changePagesCount(QVariant)));

    connect(mainView, SIGNAL(zoomValueChanged(int)), this, SLOT(onZoomValueChanged(int)));
}

void PageStatusBarController::onZoomValueChanged(int value)
{
    qDebug("onZoomValueChanged %d", value);
}

void PageStatusBarController::onCurrentPageNumberChanged(int number)
{
    emit currentPageNumberChanged(QVariant::fromValue(number));
}

void PageStatusBarController::onPagesCountChanged(int count)
{
    emit pagesCountChanged(QVariant::fromValue(count));
}

