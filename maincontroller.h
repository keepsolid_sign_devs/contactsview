#ifndef MAINCONTROLLER_H
#define MAINCONTROLLER_H

#include <QObject>
#include <QQmlApplicationEngine>

#include "contactscontroller.h"
#include "addcontactcontroller.h"
#include "manageparticipantscontroller.h"
#include "choosesignercontroller.h"
#include "pagestatusbarcontroller.h"
#include "documentinfocontroller.h"
#include "documenttitlebarcontroller.h"

class MainController : public QObject
{
    Q_OBJECT

public:
    explicit MainController(QObject* _parent = 0);
    ~MainController();

private:
    void registerTypes();
    void setupUI();
    void setupConnections();

signals:
    void addedContact(ContactModel* contact);
    void currentPageNumberChanged(int);
    void pagesCountChanged(int);

public slots:
    void onOpenContactBook();
    void onShowDocumentTitleBar(int documentId);
    void onShowContactsScreen();
    void onShowPageStatusBar();
    void onShowAddContactDialog();
    void onShowChooseSignerDialog();
    void onShowManageParticipantsDialog(int documentId);
    void onShowDocumentInfo(int documentId);
    void onSignerChosen(int contactId);

private:
    QQmlApplicationEngine engine;
    QObject* mainView;
    ContactListModel contactsModel;

    //controllers
    ContactsController* contactsController;
    AddContactController* addContactController;
    ChooseSignerController* chooseSignerController;
    ManageParticipantsController* manageParticipantsController;
    PageStatusBarController* pageStatusBarController;
    DocumentInfoController* documentInfoController;
    DocumentTitleBarController* documentTitleBarController;
};

#endif // MAINCONTROLLER_H
