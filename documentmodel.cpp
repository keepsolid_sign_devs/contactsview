#include "documentmodel.h"

DocumentModel::DocumentModel(QObject *parent) : QObject(parent)
{
}

QString DocumentModel::docID() const
{
    return m_docId;
}

QString DocumentModel::name() const
{
    return m_name;
}

int DocumentModel::docStatus() const
{
    return m_status;
}

QDate DocumentModel::creationDate() const
{
    return m_creationDate;
}

QString DocumentModel::message() const
{
    return m_message;
}

bool DocumentModel::hasOrder() const{
    return m_hasOrder;
}

bool DocumentModel::editable() const{
    return m_editable;
}

void DocumentModel::setName(QString name)
{
    if (m_name == name)
        return;

    m_name = name;
    emit nameChanged(m_name);
}
void DocumentModel::setStatus(int status)
{
    if (m_status == status)
        return;

    m_status = status;
    emit statusChanged(m_status);
}
void DocumentModel::setCreationDate(QDate creationDate)
{
    m_creationDate = creationDate;
}

void DocumentModel::setMessage(QString message)
{
    if (m_message == message)
        return;

    m_message = message;
    emit messageChanged(m_message);
}


void DocumentModel::setHasOrder(bool hasOrder)
{
    if (m_hasOrder == hasOrder)
        return;

    m_hasOrder = hasOrder;
    emit hasOrderChanged(m_hasOrder);
}

void DocumentModel::setEditable(bool editable)
{
    if (m_editable == editable)
        return;

    m_editable = editable;
    emit editableChanged(m_editable);
}

bool DocumentModel::canNotify(int participantId)
{
    qDebug("canNotify? %d", participantId);
    if (participantId % 3 == 0) {
        return true;
    }
    return false;
}
