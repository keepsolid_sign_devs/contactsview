#ifndef DOCUMENTINFOCONTROLLER_H
#define DOCUMENTINFOCONTROLLER_H

#include <QObject>
#include <QQuickView>
#include <QQmlContext>
#include <QQmlComponent>
#include <QQmlApplicationEngine>

#include "contactmodel.h"
#include "contactlistmodel.h"
#include "suggestionlistmodel.h"

class DocumentInfoController: public QObject
{
    Q_OBJECT

public:
    explicit DocumentInfoController(QObject* _parent = 0);
    DocumentInfoController(int documentId, QObject* _parent = 0);
    ~DocumentInfoController();

private:
    void registerTypes();
    void fillData();
    void setupUI();
    void setupConnections();

signals:
    void signOrderChanged(bool isImportant);


public slots:
    void onClosed();

private:
    ContactListModel observersModel;
    ContactListModel signersModel;
    QObject* mainView;
    QQmlApplicationEngine engine;
    const int SIGNER = 0;
    const int OBSERVER = 1;

};

#endif // DOCUMENTINFOCONTROLLER_H
