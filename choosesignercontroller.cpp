#include <QStyle>
#include <QTimer>
#include <QQuickItem>
#include <QApplication>
#include <QDesktopWidget>
#include <QQmlContext>
#include <QCompleter>
#include <QTextEdit>
#include <random>
#include "choosesignercontroller.h"

ChooseSignerController::ChooseSignerController(QObject* _parent)
    : QObject(_parent)
{
    registerTypes();
    fillData();
    setupUI();
    setupConnections();
}

ChooseSignerController::~ChooseSignerController()
{

}

void ChooseSignerController::registerTypes()
{
    qmlRegisterType<ContactModel>("com.contacts", 1, 0, "ContactModel");
    qmlRegisterType<ContactListModel>("com.contacts", 1, 0, "ContactListModel");
    qmlRegisterType<SuggestionListModel>("com.contacts", 1, 0, "SuggestionListModel");
    qmlRegisterType<SuggestionModel>("com.contacts", 1, 0, "SuggestionModel");
}

void ChooseSignerController::fillData()
{
    long id = 1;
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "JohnJohnJohn", "Doe", "example@gmail.com", "KeepSolidDoe", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "JohnJohnJohn", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "JohnJohnJohn", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "JohnJohnJohn", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "JohnJohnJohn", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "sad_person", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "sad_person", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "sad_person", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "sad_person", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));

    suggestionListModel.addSuggestion(id++, "example@gmail.com 1");
    suggestionListModel.addSuggestion(id++, "bexample@gmail.com 2");
    suggestionListModel.addSuggestion(id++, "exaaample@gmail.com 3");
    suggestionListModel.addSuggestion(id++, "example@gmail.com 4");
    suggestionListModel.addSuggestion(id++, "example@gmail.com 5");
    suggestionListModel.addSuggestion(id++, "example@gmail.com 6");
}

void ChooseSignerController::setupUI()
{
    mainView = new QQuickView();
    mainView->rootContext()->setContextProperty("contactListModel", &contactsModel);
    mainView->rootContext()->setContextProperty("suggestionListModel", &suggestionListModel);
    mainView->setSource(QUrl(QStringLiteral("qrc:/contacts/ChooseSignerDialog.qml")));
    mainView->setModality(Qt::ApplicationModal);
    //        mainView->setParent((QQuickView*) parent());
    mainView->setFlags(Qt::FramelessWindowHint);
    mainView->show();
    QQmlEngine::setObjectOwnership(mainView, QQmlEngine::CppOwnership);
}

void ChooseSignerController::setupConnections()
{
    QObject* closeBtn = mainView->rootObject()->findChild<QObject*>("closeBtn");
    connect(closeBtn, SIGNAL(closed()), this, SLOT(onClosed()));

    QObject* rootLayout = mainView->rootObject();
    connect(rootLayout, SIGNAL(chooseSigner(int)), this, SLOT(onChooseSigner(int)));
    connect(rootLayout, SIGNAL(addNewContact(QString)), this, SLOT(onAddNewContact(QString)));
    connect(rootLayout, SIGNAL(addSignerFromContacts(int)), this, SLOT(onAddSignerFromContacts(int)));
    connect(this, SIGNAL(startBusyIndication()), rootLayout, SLOT(startBusyIndication()));
    connect(this, SIGNAL(finishBusyIndication()), rootLayout, SLOT(finishBusyIndication()));
}

void ChooseSignerController::onChooseSigner(int contactId)
{
    emit signerChosen(contactId);
    QTimer::singleShot(1400, this, SLOT(onClosed()));
    //    onClosed();
}

void ChooseSignerController::onAddSignerFromContacts(int contactId)
{
    qDebug("onAddSignerFromContacts with id %d", contactId);
    //TODO: add signer to document
    onChooseSigner(contactId);
}

void ChooseSignerController::onAddNewContact(QString email)
{
    qDebug("onAddNewContact with email " + email.toLatin1());
    //show waiter
    emit startBusyIndication();
    //TODO: send registration request to the server
    //if result is success:
    QObject* rootLayout = mainView->rootObject();
    QTimer::singleShot(1200, rootLayout, SLOT(finishBusyIndication()));
    onAddSignerFromContacts(rand());
    //hide waiter

    //else: show error result
}

void ChooseSignerController::onClosed()
{
    qDebug("onClosed");
    mainView->close();
}

