import QtQuick 2.7
import QtQuick.Controls 2.1
import com.contacts 1.0

Rectangle {
    id: container

    // --- properties
    property SuggestionListModel model
    property int count: filterItem.model.rowCount()
    property alias currentIndex: popup.selectedIndex
    property alias currentItem: popup.selectedItem
    property alias suggestionsModel: filterItem.model
    property alias filter: filterItem.filter
    property alias property: filterItem.property
    property int maximumHeight: 100
    property bool opened: true
    signal itemSelected(variant item)

    onModelChanged: filterItem.sourceModel = model
    // --- behaviours
    z: parent.z + 100
    visible: filter.length > 0 && suggestionsModel.rowCount() > 0 && !filterMatchesLastSuggestion() && opened
    height: visible ? popup.height + border.width * 2 : 0

    Behavior on height {
        NumberAnimation{}
    }
    function filterMatchesLastSuggestion() {
        return suggestionsModel.rowCount() == 1 && suggestionsModel.itemAt(0).name === filter
    }

    FontLoader {
        id: openSansRegular
        source: "../OpenSansFonts/OpenSans-Regular.ttf"
    }

    // --- defaults
    color: "white"
    border {
        width: 1
        color: "#bfe5ff"
    }


    Filter {
        id: filterItem
//        sourceModel: model
    }

    Flickable {
        id: flickable
        clip: true
        contentWidth: popup.width
        contentHeight: popup.implicitHeight
        height: popup.height
        width: popup.width
        anchors.centerIn: parent

        flickableDirection: Flickable.VerticalFlick
        boundsBehavior: Flickable.StopAtBounds

        // --- UI
        Column {
            id: popup
            height: childrenRect.height > maximumHeight? maximumHeight - container.border.width * 2 : childrenRect.height - container.border.width * 2
            width: container.width - 2
            smooth: true

            property int selectedIndex: -1
            property variant selectedItem: selectedIndex === -1 ? undefined : suggestionsModel.itemAt(selectedIndex)
            signal suggestionClicked(variant suggestion)

            opacity: container.visible ? 1.0 : 0
            Behavior on opacity {
                NumberAnimation { }
            }

            Repeater {
                id: repeater
                model: container.suggestionsModel
                delegate: Item {
                    id: delegateItem
                    property bool keyboardSelected: popup.selectedIndex === suggestion.index
                    property bool selected: itemMouseArea.containsMouse
                    property variant suggestion: model

                    height: 24
                    width: parent.width

                    onKeyboardSelectedChanged: {
                        flickable.contentY = flickable.contentY + popup.height < popup.selectedIndex * 24 + 24? popup.selectedIndex * 24 + 24 - popup.height
                                                    : flickable.contentY > popup.selectedIndex * 24? popup.selectedIndex * 24
                                                         :flickable.contentY
                    }

                    Rectangle {
                        height: parent.height
                        color: delegateItem.selected || delegateItem.keyboardSelected? "#f8f8f8" : "white"
                        width: parent.width
                        Text {
                            id: textComponent
                            width: parent.width
                            height: parent.height
                            leftPadding: 7
                            rightPadding: 6
                            color: "#444444"
                            font.pointSize: 12
                            font.family: openSansRegular.name
                            elide: Text.ElideRight
                            wrapMode: Text.WordWrap
                            textFormat: Text.StyledText
                            text: {
                                var matchIndex = suggestion.name.indexOf(filter)
                                var matchLength = filter.length
                                text = suggestion.name.substring(0, matchIndex) + "<b>"
                                        + suggestion.name.substring(matchIndex, matchIndex + matchLength) + "</b>"
                                        + suggestion.name.substring(matchIndex + matchLength, suggestion.name.length)
                            }

                            clip: true
                            verticalAlignment: Qt.AlignVCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                    }

                    MouseArea {
                        id: itemMouseArea
                        anchors.fill: parent
                        hoverEnabled: true
                        onClicked: container.itemSelected(delegateItem.suggestion)
                    }
                }
            }
        }
    }
}

