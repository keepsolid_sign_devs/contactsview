import QtQuick 2.7
import com.contacts 1.0

Item {
    id: component
    property alias model: filterModel

    property SuggestionListModel sourceModel
    property string filter: ""
    property string property: ""
    onFilterChanged: invalidateFilter()
    onPropertyChanged: invalidateFilter()
    onSourceModelChanged: invalidateFilter()

    Component.onCompleted: invalidateFilter()

    SuggestionListModel {
        id: filterModel
    }

    // filters out all items of source model that does not match filter
    function invalidateFilter() {
        if (sourceModel === undefined)
            return;
        filterModel.clear();

        if (!isFilteringPropertyOk())
            return
        var length = sourceModel.rowCount()
        for (var i = 0; i < length; ++i) {
            var item = sourceModel.itemAt(i);
            if (isAcceptedItem(item)) {
                filterModel.addSuggestion(item.suggestionId, item.name)
            }
        }
    }


    // returns true if item is accepted by filter
    function isAcceptedItem(item) {
        if (item === null || item === undefined) {
            return false
        }

        if (item[this.property] === undefined)
            return false

        if (item[this.property].match(this.filter) === null) {
            return false
        }

        return true
    }

    // checks if it has any sence to process invalidating based on property
    function isFilteringPropertyOk() {
        if(this.property === undefined || this.property === "") {
            return false
        }
        return true
    }
}

