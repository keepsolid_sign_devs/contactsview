import QtQuick 2.7
import "." as TextViews
import com.contacts 1.0

Rectangle {
    id: inputLayout
    property SuggestionListModel suggestionsModel: undefined
    property alias inputField: inputField
    signal actionBtnClicked()
    signal suggestionChosen(variant item)
    signal userInputChosen(variant item)

    onSuggestionsModelChanged: suggestionsBox.model = suggestionsModel
    activeFocusOnTab: true

    onFocusChanged: {
        if (focus) {
            showSuggestions()
        } else {
            hideSuggestions()
        }
    }

    Keys.onUpPressed: inputField.activateSuggestionAt(-1)
    Keys.onDownPressed: inputField.activateSuggestionAt(+1)
    Keys.onReturnPressed: inputField.processEnter()
    Keys.onEscapePressed: hideSuggestions()

    width: parent.width * 0.8
    height: inputField.height

    function hideSuggestions() {
        console.log("hideSuggestions")
        suggestionsBox.currentIndex = -1
        suggestionsBox.opened = false
    }

    function showSuggestions() {
        console.log("showSuggestions")
        if (!suggestionsBox.opened) {
            suggestionsBox.opened = true
        }
    }

    Item {
        id: contents
        width: parent.width
        height: parent.height
        anchors.centerIn: parent

        TextViews.InputField {
            id: inputField
            width: parent.width

            Keys.onUpPressed: activateSuggestionAt(-1)
            Keys.onDownPressed: activateSuggestionAt(+1)
            Keys.onReturnPressed: processEnter()
            Keys.onEscapePressed: hideSuggestions()
            onActionBtnClicked: inputLayout.actionBtnClicked()

            onTextChanged: {
                showSuggestions()
            }

            onStateChanged: {
                if (state != "Active") {
                    hideSuggestions()
                }
            }

            function activateSuggestionAt(offset) {
                var max = suggestionsBox.count
                if(max == 0 || suggestionsBox.filter.length==0)
                    return
                var newIndex = ((suggestionsBox.currentIndex + 1 + offset) % (max + 1)) - 1
                if (newIndex > -1 && newIndex != suggestionsBox.count + 1) {
                    suggestionsBox.currentIndex = newIndex
                    console.log("activateSuggestionAt " + newIndex)
                }
                inputLayout.forceActiveFocus()
            }

            function processEnter() {
                if (suggestionsBox.currentIndex === -1) {
                    hideSuggestions()
                    userInputChosen(text)
                } else {
                    var chosenItem = suggestionsBox.currentItem
                    suggestionsBox.complete(chosenItem)
                    suggestionChosen(chosenItem)
                    hideSuggestions()
                }
            }
        }

        TextViews.SuggestionBox {
            id: suggestionsBox
            width: parent.width
            maximumHeight: inputLayout.parent.y + inputLayout.parent.height - suggestionsBox.y - 90
            anchors.top: inputField.bottom
            clip: true
            anchors.topMargin: -1
            anchors.left: inputField.left
            filter: inputField.text
            property: "name"
            onItemSelected: complete(item)
//            modelModel: suggestionsModel

            function complete(item) {
                if (item !== undefined) {
                    inputField.text = item.name
                }
            }
        }
    }
}
