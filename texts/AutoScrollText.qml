import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Flickable{
    id: textContainer

    property alias contentTextWidth: textContainer.contentWidth
    property alias contentTextHeight: textContainer.contentHeight
    property alias contentText: scrollingText
    property int  maxNotScrolledLines

    property color backdroundColor: "transparent"
    property color borderColor: scrollingText.activeFocus ? "#bfe5ff" : "#ebebeb"

    property alias animation: scrollAnimation
    property bool editable: false

    readonly property bool scrollbarNeeded: maxNotScrolledLines * fontMetrics.height < scrollingText.height

    property int  calculatedHeight: maxNotScrolledLines ? (maxNotScrolledLines * fontMetrics.height > scrollingText.paintedHeight ? scrollingText.paintedHeight : maxNotScrolledLines * fontMetrics.height) : parent.height

    height: maxNotScrolledLines ? calculatedHeight : parent.height

    function startAnimation(){
        scrollingText.y = 0
        scrollAnimation.start()
    }

    function stopAnimation(){
        scrollingText.y = 0
        scrollAnimation.stop()
    }

    flickableDirection: Flickable.VerticalFlick
    boundsBehavior: Flickable.StopAtBounds
    clip: true

    Rectangle{
        anchors.fill: parent
        color: backdroundColor
    }

    ScrollBar.vertical:  ScrollBar {
        id: scrollBar
        visible: scrollbarNeeded
    }

    FontMetrics {
        id: fontMetrics
        font.family: scrollingText.font.family
        font.pointSize: scrollingText.font.pointSize
    }

    TextEdit{
        id: scrollingText
        width: scrollingText.width
        wrapMode: Text.WordWrap
        textFormat: Text.StyledText
        anchors.right: parent.right
        anchors.left: parent.left
        font.family: "OpenSans"
        color: primaryTextColor
        font.pointSize: 12
        enabled: editable
        leftPadding: 10
        rightPadding: 6

        NumberAnimation on y{
            id: scrollAnimation
            from: 0
            to: -1*scrollingText.height
            loops: Animation.Infinite
            running: false
            duration: 8000
        }
    }

}
