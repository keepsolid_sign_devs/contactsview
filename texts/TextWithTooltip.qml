import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1

Text {
    id: field
    clip: true
    maximumLineCount: 3
    wrapMode: Text.WordWrap
    textFormat: Text.PlainText
    elide: Text.ElideRight
    font.family: openSansRegular.name

    FontLoader {
        id: openSansRegular
        source: "../OpenSansFonts/OpenSans-Regular.ttf"
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        ToolTip {
            visible: mouseArea.containsMouse && textMetrics.boundingRect.width > field.width
            text: field.text
            y: mouseArea.mouseY + 20
            x: mouseArea.mouseX
            delay: 1000
            timeout: 5000
        }
    }

    TextMetrics {
        id: textMetrics
        font.pointSize: field.font.pointSize
        elide: field.elide
        font.family: field.font.family
        text: field.text
    }

}
