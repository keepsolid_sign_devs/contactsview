import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4

Item {
    id: root
    readonly property color borderColor: "#ebebeb"
    readonly property color disabledBackgroundColor: "#fafafa"
    readonly property color disabledBorderColor: "#f3f3f3"
    readonly property color activeBorderColor: "#bfe5ff"
    readonly property color warningBorderColor: "#f6806b"
    readonly property color primaryTextColor: "#444444"
    readonly property color warningTextColor: "#f02b2b"
    readonly property color hintTextColor: "#8e8e8e"
    readonly property color selectionColor: "#2980cc"
    readonly property bool hasWarnings: warningText!=""
    property int actionBtnSize: 8
    readonly property int defaultIconSize: 8
    readonly property string defaultIconSource: "../contacts/iconWindowCloseDark"

    property alias textEdit: textEdit
    property alias actionBtnSource: actionBtnIcon.source
    property alias actionBtnVisible: actionBtn.visible
    property alias fieldIconSource: fieldIcon.source
    property alias fieldIconVisible: fieldIcon.visible
    property alias warningText: fieldWarning.text
    property alias disabled: textEdit.readOnly
    property alias busy: busyIndicator.running

    property alias text: textEdit.text
    property bool isPassword: false
    property string hintText: ""
    property bool isFieldFocused

    signal finishEdit(string textResult)
    signal actionBtnClicked()

    Layout.preferredHeight: height
    height: etContainer.height + fieldWarning.height
    focus: true
    activeFocusOnTab: true

    FontLoader {
        id: openSansRegular
        source: "../OpenSansFonts/OpenSans-Regular.ttf"
    }

    states: [
        State {
            name: "Busy"
            when: busy
            PropertyChanges { target: etContainer; border.color: disabledBorderColor }
            PropertyChanges { target: etContainer; color: disabledBackgroundColor }
            PropertyChanges { target: textEdit; opacity: 0.5 }
            PropertyChanges { target: hint; opacity: 0.5 }
            PropertyChanges { target: actionBtn; visible: false }
        },
        State {
            name: "Disabled"
            when: disabled
            PropertyChanges { target: etContainer; border.color: disabledBorderColor }
            PropertyChanges { target: etContainer; color: disabledBackgroundColor }
            PropertyChanges { target: textEdit; opacity: 0.5 }
            PropertyChanges { target: hint; opacity: 0.5 }
        },
        State {
            name: "Active"
            when: textEdit.activeFocus | isFieldFocused
            PropertyChanges { target: etContainer; border.color: activeBorderColor }
            PropertyChanges { target: fieldWarning; text: ""; restoreEntryValues: false }
            PropertyChanges { target: actionBtnIcon; visible: text!="" }
            PropertyChanges { target: actionBtnIcon; source: defaultIconSource }
            PropertyChanges { target: actionBtnIcon; opacity: 0.5 }
            PropertyChanges { target: root; actionBtnSize: defaultIconSize }
        },
        State {
            name: "Error"
            when: hasWarnings
            PropertyChanges { target: etContainer; border.color: warningBorderColor }
            PropertyChanges { target: textEdit; color: warningTextColor }
            PropertyChanges { target: hint; color: warningTextColor }
        },
        State {
            name: "Placeholder"
            when: !textEdit.activeFocus
            PropertyChanges { target: etContainer; border.color: borderColor }
        }
    ]

    Rectangle {
        id: etContainer
        width: parent.width
        height: 32

        BusyIndicator {
            id: busyIndicator
            implicitHeight: 32
            implicitWidth: 32
            focusPolicy: Qt.WheelFocus
            anchors.centerIn: parent
            running: false
            z: 100
        }

        Image {
            id: fieldIcon
            width: 14
            height: 14
            sourceSize.height: 14
            sourceSize.width: 14
            visible: false
            anchors { verticalCenter: parent.verticalCenter; left: parent.left; leftMargin: 10 }
        }

        TextInput {
            id: textEdit
            height: parent.height
            Layout.fillWidth: true
            Layout.fillHeight: true
            verticalAlignment: Text.AlignVCenter
            font.family: openSansRegular.name
            anchors {
                right: actionBtn.visible? actionBtn.left : parent.right
                left: fieldIcon.visible? fieldIcon.right : parent.left
                top: parent.top; bottom: parent.bottom; rightMargin: 8; leftMargin: 8
            }
            font.pointSize: 12
            color: primaryTextColor
            clip: true
            selectByMouse: true
            selectionColor: root.selectionColor
            echoMode: isPassword ? TextInput.Password : TextInput.Normal

            onFocusChanged: {
                isFieldFocused = !focus && actionBtn.activeFocus
            }
            onTextChanged: {
                warningText = ""
            }
        }

        Text {
            id: hint
            height: parent.height
            Layout.fillWidth: true
            Layout.fillHeight: true
            verticalAlignment: Text.AlignVCenter
            font.family: openSansRegular.name
            color: hintTextColor
            text: textEdit.text!=""? "" : hintText
            anchors {
                right: actionBtn.visible? actionBtn.left : parent.right
                left: fieldIcon.visible? fieldIcon.right : parent.left
                top: parent.top; bottom: parent.bottom; rightMargin: 4; leftMargin: 8
            }
            font.pointSize: 12
            clip: true
            focus: false
        }

        AbstractButton {
            id: actionBtn
            width: 14
            height: parent.height
            anchors { right: parent.right; rightMargin: 10 }

            Image {
                id: actionBtnIcon
                width: actionBtnSize
                height: actionBtnSize
                sourceSize.height: actionBtnSize
                sourceSize.width: actionBtnSize
                anchors { verticalCenter: parent.verticalCenter; horizontalCenter: parent.horizontalCenter }
            }

            onClicked: {
                if (isFieldFocused) {
                    textEdit.text=""
                    textEdit.focus = true
                } else {
                    actionBtnClicked()
                }

            }
        }
    }

    Label {
        id: fieldWarning
        height: visible? implicitHeight : 0
        anchors { top: etContainer.bottom; topMargin: 2; right: parent.right }
        Layout.alignment: Qt.AlignRight
        color: warningTextColor
        font.pointSize: 11
        visible: text!=""
    }

    //doesnt work now?
    Keys.onReturnPressed : {
        nextItemInFocusChain().forceActiveFocus()
        finishEdit(textEdit.text)
    }
    Keys.onDownPressed: nextItemInFocusChain().forceActiveFocus()
    Keys.onUpPressed:  nextItemInFocusChain(false).forceActiveFocus()
}
