#ifndef CHOOSESIGNERCONTROLLER_H
#define CHOOSESIGNERCONTROLLER_H

#include <QObject>
#include <QQuickView>
#include <QQmlApplicationEngine>

#include "contactmodel.h"
#include "contactlistmodel.h"
#include "suggestionlistmodel.h"

class ChooseSignerController: public QObject
{
    Q_OBJECT

public:
    explicit ChooseSignerController(QObject* _parent = 0);
    ~ChooseSignerController();

private:
    void registerTypes();
    void fillData();
    void setupUI();
    void setupConnections();

signals:
    void signerChosen(int contactId);
    void startBusyIndication();
    void finishBusyIndication();

public slots:
    void onChooseSigner(int contactId);
    void onAddSignerFromContacts(int contactId);
    void onAddNewContact(QString email);
    void onClosed();

private:
    SuggestionListModel suggestionListModel;
    ContactListModel contactsModel;
    QQuickView* mainView;
    QQmlApplicationEngine engine;

};


#endif // CHOOSESIGNERCONTROLLER_H
