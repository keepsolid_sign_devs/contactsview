#include "maincontroller.h"
#include <QQuickWindow>
#include <QApplication>
#include <QStyle>
#include <QDesktopWidget>
#include <QGraphicsBlurEffect>

MainController::MainController(QObject* _parent) : QObject(_parent)
{
    registerTypes();
    setupUI();
    setupConnections();
}

MainController::~MainController()
{
    delete contactsController;
    delete chooseSignerController;
    delete addContactController;
    delete manageParticipantsController;
    delete pageStatusBarController;
    delete documentTitleBarController;
}

void MainController::registerTypes()
{
    qmlRegisterType<ContactModel>("com.contacts", 1, 0, "ContactModel");
    qmlRegisterType<DocumentModel>("com.contacts", 1, 0, "DocumentModel");
    qmlRegisterType<ContactListModel>("com.contacts", 1, 0, "ContactListModel");
    qmlRegisterType<ContactSortFilterModel>("com.contacts", 1, 0, "ContactSortFilterModel");
    qmlRegisterType<QGraphicsBlurEffect>("Effects", 1, 0, "Blur");
}
void MainController::setupUI()
{

    long id = 1;
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));

    contactsModel.addContact(id++, "JohnJohnJohn", "Doe", "example@gmail.com", "KeepSolidDoe", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "JohnJohnJohn", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "sad_person", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "sad_person", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "sad_person", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));


    engine.rootContext()->setContextProperty("contactListModel", &contactsModel);

    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return;

    mainView = engine.rootObjects().first();
    ((QQuickWindow*) mainView)->setGeometry(
                QStyle::alignedRect(
                    Qt::LeftToRight,
                    Qt::AlignCenter,
                    ((QQuickWindow*) mainView)->size(),
                    qApp->desktop()->availableGeometry()
                    )
                );
}

void MainController::setupConnections()
{
    QObject *footer = mainView->findChild<QObject*>(QString("Footer"));

    QObject::connect(footer, SIGNAL(syncButtonClicked()), this, SLOT(onShowChooseSignerDialog()));
}

void MainController::onShowDocumentTitleBar(int documentId)
{
    documentTitleBarController = new DocumentTitleBarController(documentId, mainView->findChild<QObject*>("documentTitleBar"), mainView);
}

void MainController::onShowContactsScreen()
{
    contactsController = new ContactsController(mainView->findChild<QObject*>("container"));

    QObject *searchView = mainView->findChild<QObject*>(QString("searchView"));
    QObject *footer = mainView->findChild<QObject*>(QString("Footer"));

    QObject::connect(searchView, SIGNAL(searchTextChanged(QString)), contactsController, SLOT(onFilterContacts(QString)));
    QObject::connect(footer, SIGNAL(addButtonClicked()), this, SLOT(onShowAddContactDialog()));
    //    QObject::connect(this, SIGNAL(addedContact()), contactsController, SLOT(onAddedContact());
}

void MainController::onShowPageStatusBar()
{
    pageStatusBarController = new PageStatusBarController(mainView->findChild<QObject*>("statusBar"));

    QObject::connect(this, SIGNAL(currentPageNumberChanged(int)), pageStatusBarController, SLOT(onCurrentPageNumberChanged(int)));
    QObject::connect(this, SIGNAL(pagesCountChanged(int)), pageStatusBarController, SLOT(onPagesCountChanged(int)));
}

void MainController::onShowAddContactDialog()
{
    addContactController = new AddContactController(mainView);
    QObject::connect(addContactController, SIGNAL(addedContact(ContactModel*)), contactsController, SLOT(onAddedContact(ContactModel*)));
    QObject::connect(addContactController, SIGNAL(openContactBook()), this, SLOT(onOpenContactBook()));
}

void MainController::onShowChooseSignerDialog()
{
    chooseSignerController = new ChooseSignerController(mainView);
    QObject::connect(chooseSignerController, SIGNAL(signerChosen(int)), this, SLOT(onSignerChosen(int)));
}

void MainController::onShowManageParticipantsDialog(int documentId)
{
    manageParticipantsController = new ManageParticipantsController(documentId, mainView->findChild<QObject*>("panel"));
}

void MainController::onShowDocumentInfo(int documentId)
{
    documentInfoController = new DocumentInfoController(documentId, mainView->findChild<QObject*>("panel"));
}

void MainController::onOpenContactBook()
{
    qDebug("onOpenContactBook");
}

void MainController::onSignerChosen(int contactId)
{
    qDebug("onSignerChosen with id %d", contactId);
}

