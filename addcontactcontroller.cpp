#include <QApplication>
#include <QStyle>
#include <QQuickItem>
#include <QQuickView>
#include <QTimer>
#include <QDesktopWidget>

#include "addcontactcontroller.h"

AddContactController::AddContactController(QObject* _parent) : QObject(_parent)
{
    registerTypes();
    setupUI();
    setupConnections();
}

AddContactController::~AddContactController()
{
    delete mainView;
}

void AddContactController::registerTypes()
{
    qmlRegisterType<ContactModel>("com.contacts", 1, 0, "ContactModel");
}

void AddContactController::setupUI()
{
    mainView = new QQuickView();
    mainView->setSource(QUrl(QStringLiteral("qrc:/contacts/AddContactDialog.qml")));
    mainView->setModality(Qt::ApplicationModal);
    //        mainView->setParent((QQuickView*) parent());
    mainView->setFlags(Qt::FramelessWindowHint);
    mainView->show();
    QQmlEngine::setObjectOwnership(mainView, QQmlEngine::CppOwnership);
}

void AddContactController::setupConnections()
{
    QObject* rootLayout = mainView->rootObject();
    connect(this, SIGNAL(startBusyIndication()), rootLayout, SLOT(startBusyIndication()));
    connect(this, SIGNAL(finishBusyIndication()), rootLayout, SLOT(finishBusyIndication()));

    QObject* closeBtn = mainView->rootObject()->findChild<QObject*>("closeBtn");
    connect(closeBtn, SIGNAL(closed()), this, SLOT(onClosed()));

    QObject* emailField = mainView->rootObject()->findChild<QObject*>("emailField");
    connect(emailField, SIGNAL(actionBtnClicked()), this, SLOT(onOpenContactBook()));

    QObject* addBtn = mainView->rootObject()->findChild<QObject*>("addBtn");
    connect(addBtn, SIGNAL(addContact(ContactModel*)), this, SLOT(onAddContact(ContactModel*)));
}

bool AddContactController::validateFields()
{
    qDebug("validateFields");
    return true;
}

void AddContactController::onAddContact(ContactModel* contact)
{
    qDebug("onAddContact");
    //send registration request to server
    //if success, emit added contact and finish busy indication
    startBusyIndication();
    QObject* rootLayout = mainView->rootObject();
    QTimer::singleShot(1200, rootLayout, SLOT(finishBusyIndication()));
    QTimer::singleShot(1400, this, SLOT(onClosed()));
    emit addedContact(contact);
    //else show warning dialog
}

void AddContactController::onOpenContactBook()
{
    emit openContactBook();
}

void AddContactController::onClosed() {
    qDebug("onClosed");
    mainView->close();
}
