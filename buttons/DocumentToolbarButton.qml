import QtQuick 2.7
import "."

ImageButton{
    height: parent.height
    width: 32
    anchors { verticalCenter: parent.verticalCenter }
    hoveredColor: "#e6e6e6"
    image.height: 12
    image.width: 12
}
