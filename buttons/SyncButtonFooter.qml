import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3

AbstractButton {
    id: syncButtonFooter

    FontLoader {
        id: openSansRegular
        source: "../OpenSansFonts/OpenSans-Regular.ttf"
    }

    contentItem: Text {
        text: "<b>" + "Last sync:" + "</b>" + " 7 June 2016"
        font.pointSize: 10
        font.family: openSansRegular.name
        leftPadding: 20
        color: "#5e5e5e"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }
    Image {
        id: syncIcon
        height: 14
        width: 14
        sourceSize.height: 14
        sourceSize.width: 14
        antialiasing: true
        fillMode: Image.PreserveAspectFit
        anchors { verticalCenter: parent.verticalCenter }
        source: "../contacts/sync"
        RotationAnimator {
            id: syncIconAnim
            target: syncIcon;
            from: 0;
            to: 360;
            loops: Animation.Infinite;
            duration: 1000
            running: false
        }
    }
    onClicked: syncIconAnim.running = !syncIconAnim.running
}
