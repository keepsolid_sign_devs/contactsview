import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1

CheckBox {
    readonly property  color  primaryColor: "#2980cc"
    readonly property  color  inactiveColor: "#ebebeb"
    readonly property  color  hoveredColor: "#f6fbff"
    readonly property  color  disabledColor: "#ebebeb"
    readonly property  color  disabledBorderColor: "#444444"
    readonly property  color  enabledTextColor: "#444444"

    id: rootCheckBox
    enabled: true
    rightPadding:  0
    leftPadding:  0

    FontLoader {
        id: openSansRegular
        source: "../OpenSansFonts/OpenSans-Regular.ttf"
    }

    states: [
        State {
            name: "Disabled"
            when: !enabled
            PropertyChanges { target: backgroundRect; border.color: disabledBorderColor; color: disabledColor }
            PropertyChanges { target: checkBoxText; color: disabledTextColor }
            PropertyChanges { target: checkRectangle; visible: false }
        },
        State {
            name: "Not Active"
            when: enabled && !checked && ! mousearea.containsMouse
            PropertyChanges { target: backgroundRect; color: inactiveColor }
            PropertyChanges { target: checkBoxText; color: enabledTextColor }
            PropertyChanges { target: checkRectangle; visible: false }
        },

        State {
            name: "Hover"
            when: enabled && mousearea.containsMouse && !checked
            PropertyChanges { target: backgroundRect; color: hoveredColor }
            PropertyChanges { target: checkBoxText; color: hoveredColor }
            PropertyChanges { target: checkBoxText; color: enabledTextColor }
            PropertyChanges { target: checkRectangle; visible: false }
        },

        State {
            name: "Checked"
            when: checked
            PropertyChanges { target: checkRectangle; visible: true }
            PropertyChanges { target: checkBoxText; color: enabledTextColor }
        }
    ]

    contentItem: Text {
        id: checkBoxText
        text: rootCheckBox.text
        font.pointSize: 11
        font.family: openSansRegular.name
        color: enabledTextColor
        opacity: enabled ? 1.0 : 0.4
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.Wrap
        leftPadding: rootCheckBox.indicator.width + rootCheckBox.spacing
    }

    indicator: Rectangle {
        id: backgroundRect
        implicitWidth: 16
        implicitHeight: 16
        x: parent.leftPadding
        y: parent.height / 2 - height / 2
        border.color: primaryColor
        opacity: enabled ? 1.0 : 0.4
        color: inactiveColor

        MouseArea{
            id: mousearea
            hoverEnabled: true
            anchors.fill: backgroundRect
            onClicked: {
                rootCheckBox.checked = !rootCheckBox.checked
                console.log("checked " + rootCheckBox.checked)
            }

            Rectangle {
                id: checkRectangle
                anchors.fill: parent
                color: primaryColor
                Image{
                    anchors.fill: parent
                    source: "/contacts/ic_check"
                }
            }
        }

    }
}
