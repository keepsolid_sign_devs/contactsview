import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3

AbstractButton {
    id: addButtonFooter
    property string iconSrc: "http://www.iconsdb.com/icons/download/tropical-blue/plus-4-128.png"
    property string contentText: "Add"
    property int contentPadding: 10

    width: addBtnText.implicitWidth + addBtnImage.implicitWidth + contentPadding * 2
    height: 36

    background: Rectangle {
        color: addButtonFooter.down? "#f6fbff" : addButtonFooter.hovered? "#f6fbff" : "transparent"
        border.color: addButtonFooter.down? "#e2f1fd" : addButtonFooter.hovered? "#e2f1fd" : "transparent"
        border.width: 1
        anchors.fill: parent
    }

    Image {
        id: addBtnImage
        width: 16
        height: 16
        sourceSize.width: 16
        sourceSize.height: 16
        anchors { verticalCenter: parent.verticalCenter; left: parent.left; leftMargin: contentPadding }
        source: iconSrc
    }

    Label {
        id: addBtnText
        width: Layout.implicitWidth
        height: parent.height
        color: "#2980cc"
        padding: contentPadding
        text: contentText
        font.pointSize: 13
        verticalAlignment: Text.AlignVCenter
        anchors { left: addBtnImage.right }
    }

    Behavior on opacity {
        OpacityAnimator { duration: 100 }
    }
}
