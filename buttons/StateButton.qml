import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import "../styles/"

Button {
    id: stateButton
    property alias contentText: btnText.text

    property ButtonStateStyle stateStyle: Styles.primary //set this property to choose corresponding style for button*/
    property double disabledOpacity: 0.4

    states: [
        State {
            name: "Disabled"
            when: !enabled
            PropertyChanges { target: background; color: stateStyle.disabledColorBkg }
            PropertyChanges { target: stateButton; opacity: disabledOpacity }
        },
        State {
            name: "Clicked"
            when: down
            PropertyChanges { target: background; color: stateStyle.clickedColorBkg }
        },
        State {
            name: "Hover"
            when: hovered
            PropertyChanges { target: background; color: stateStyle.hoveredColorBkg }
        }
    ]

    flat: true
    background: Rectangle {
        id: background
        implicitWidth: 101
        implicitHeight: 36
        color: stateStyle? stateStyle.normalColorBkg : Styles.primary.normalColorBkg
    }
    Label {
        id: btnText
        width: parent.width
        height: parent.height
        padding: 10
        color: stateStyle? stateStyle.textColor : Styles.primary.textColor
        font.pointSize: 13
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: TextInput.AlignHCenter
    }
}
