import QtQuick 2.7
import QtQuick.Controls 2.1

AbstractButton {
    property alias image: actionBtnIcon
    property string hoveredColor
    property string hoveredImageSource

    id: actionBtn
    width: 14
    height: 14

    states: [
        State {
            when: hovered
            PropertyChanges { target: backgroundRect; color: hoveredColor? hoveredColor: "transparent" }
            PropertyChanges { target: actionBtnIcon; source: hoveredImageSource? hoveredImageSource: actionBtnIcon.source }
        }

    ]
    background: Rectangle {
        id: backgroundRect
        color: "transparent"
    }

    Image {
        id: actionBtnIcon
        width: actionBtn.width
        height: actionBtn.width
        sourceSize.height: width
        sourceSize.width: height
        antialiasing: true
        anchors { verticalCenter: parent.verticalCenter; horizontalCenter: parent.horizontalCenter }
    }
}
